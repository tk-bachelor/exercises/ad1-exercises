/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 23 10:36:58 CEST 2016
 */

package uebung13.aufgabe02;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.io.*;

public class UIDGenerator {

  public static final String SAVE_FILE = "Uebungen/uebung13/aufgabe02/uid.ser";

  public static final int UID_LENGTH = 8;

  private Map<String, String> map;

  public UIDGenerator(Map<String, String> m) {
    map = m;
  }

  /**
   * Replaces all mutated vowels with the correspondig two-vocal-replacment
   * (e.g. "ä" -> "ae").
   * 
   * @param s
   *          String which potentially contains mutated vowels.
   * @return String with replaced mutated vowels.
   */
  private String replaceMutatedVowel(String s) {
    // ö = ou
    // ä = ae
    // ü = ue
    
    s = s.replaceAll("ä", "ae");
    s = s.replaceAll("ö", "ou");
    s = s.replaceAll("ü", "ue");
    return s;
  }
  
  /**
   * Removes all upper-case-characters and all special characters.
   * Concrete: removes all characters except: 'a'..'z'
   * 
   * @param s
   *          String which potentially contains special characters.
   * @return String with deleted special characters.
   */
  private String deleteSpecialCharacters(String s) {    
    return s.replaceAll("[^a-z]", "");
  }

  public String generateUID(String n1, String n2) {
    n1 = n1.toLowerCase();
    n2 = n2.toLowerCase();

    n1 = replaceMutatedVowel(n1);
    n2 = replaceMutatedVowel(n2);
    
    n1 = deleteSpecialCharacters(n1);
    n2 = deleteSpecialCharacters(n2);
    
    char c = n1.charAt(0);
    String uid = max8Chars(c+n2);
    
    int i = 1;
    while(map.containsKey(uid)){
      System.out.println(uid);
      uid = max8Chars(String.format("%c%d%s", c, i++, n2));
    }
    return uid;
  }
  
  private String max8Chars(String str){
    if(str.length() <= 8){
      return str;
    }
    return str.substring(0, 8);
  }

  public static void main(String[] args) throws Exception {
    
    File f = new File(SAVE_FILE);
    Map<String, String> uids;
    if (f.exists()) {
      FileInputStream fis = new FileInputStream(SAVE_FILE);
      ObjectInputStream ois = new ObjectInputStream(fis);
      uids = (Map<String, String>) ois.readObject();
      ois.close();
    } else {
      uids = new HashMap<String, String>();
    }
    
    if (args.length < 2) {
      System.out.println("Usage: java UIDGenerator first_name last_name");
      
      printUsernames(uids);
      System.exit(1);
    }
       
    UIDGenerator gen = new UIDGenerator(uids);
    String uid = gen.generateUID(args[0], args[1]);
    uids.put(uid, args[0] + " " + args[1]);
    // save map
    FileOutputStream fos = new FileOutputStream(SAVE_FILE);
    ObjectOutputStream oos = new ObjectOutputStream(fos);
    oos.writeObject(uids);
    oos.close();
    System.out.println("Username generated: " + uid);
    
    printUsernames(uids);
  }
  
  private static void printUsernames(Map<String, String> uids){
    System.out.println("Usernames so far " + uids.size() + ":");
    Set<String> keys = uids.keySet();
    for (String key : keys) {
      System.out.println(uids.get(key) + ": " + key);
    }
  }
  
}

/* Session-Log:

$ java -classpath Uebungen uebung12.as.aufgabe02.UIDGenerator John Smith
Username generated: jsmith
Usernames so far 10:
Mueller Meier: m2meier
John Smith: jsmith
Calanda Braeu: cbraeu
Anna Bolika: abolika
Hans Lueginsland: hluegins
Hannibal Hans: hhans
Heinz Ketchup: hketchup
Hans Lueginslandvoobe: h1luegin
Michael Meier: m1meier
Marco Meier: mmeier


$ xxd uid.ser                                                                         
0000000: aced 0005 7372 0011 6a61 7661 2e75 7469  ....sr..java.uti
0000010: 6c2e 4861 7368 4d61 7005 07da c1c3 1660  l.HashMap......`
0000020: d103 0002 4600 0a6c 6f61 6446 6163 746f  ....F..loadFacto
0000030: 7249 0009 7468 7265 7368 6f6c 6478 703f  rI..thresholdxp?
0000040: 4000 0000 0000 0c77 0800 0000 1000 0000  @......w........
0000050: 0a74 0007 6d32 6d65 6965 7274 000d 4d75  .t..m2meiert..Mu
0000060: 656c 6c65 7220 4d65 6965 7274 0006 6a73  eller Meiert..js
0000070: 6d69 7468 7400 0a4a 6f68 6e20 536d 6974  mitht..John Smit
0000080: 6874 0006 6362 7261 6575 7400 0d43 616c  ht..cbraeut..Cal
0000090: 616e 6461 2042 7261 6575 7400 0761 626f  anda Braeut..abo
00000a0: 6c69 6b61 7400 0b41 6e6e 6120 426f 6c69  likat..Anna Boli
00000b0: 6b61 7400 0868 6c75 6567 696e 7374 0010  kat..hlueginst..
00000c0: 4861 6e73 204c 7565 6769 6e73 6c61 6e64  Hans Lueginsland
00000d0: 7400 0568 6861 6e73 7400 0d48 616e 6e69  t..hhanst..Hanni
00000e0: 6261 6c20 4861 6e73 7400 0868 6b65 7463  bal Hanst..hketc
00000f0: 6875 7074 000d 4865 696e 7a20 4b65 7463  hupt..Heinz Ketc
0000100: 6875 7074 0008 6831 6c75 6567 696e 7400  hupt..h1luegint.
0000110: 1548 616e 7320 4c75 6567 696e 736c 616e  .Hans Lueginslan
0000120: 6476 6f6f 6265 7400 076d 316d 6569 6572  dvoobet..m1meier
0000130: 7400 0d4d 6963 6861 656c 204d 6569 6572  t..Michael Meier
0000140: 7400 066d 6d65 6965 7274 000b 4d61 7263  t..mmeiert..Marc
0000150: 6f20 4d65 6965 7278                      o Meierx

*/


