package uebung01;


public class ArithmeticSequence {
  public static void main(String[] args) {
    
    //(a) 1, 2, 3, 4, ...
    //(b) 5, 13, 21, 29, …
    
    int n = 5;
    // explicit
    System.out.println("Explicitely: an = " + doItExplicitely(n, 8, 3));
    
    // iterative
    System.out.println("Iterative: an = " + doItIterative(5, 8, n));
    
    // recursive
    System.out.println("Recursive: an = " + doItRecursively(5, 5, 8));
    
    
    // Summenformel
    System.out.println(sumExplicitely(n, 5, 8, 3));
    System.out.println(sumInteratively(5, 8, 5));
    System.out.println(sumRecursively(5, 5, 8));
  }

  private static int sumRecursively(int n, int start, int diff) {
    if(n == 1){
      return start;      
    }else {
      return sumRecursively(n-1, start, diff) + doItRecursively(n, start, diff);
    }
  }

  private static int sumInteratively(int start, int diff, int n) {
    int total = 0;
    for(int i=1; i <= n ; i++){
      total += doItIterative(start, diff, i);
    }
    return total;
  }

  private static int sumExplicitely(int n, int start, int diff, int c) {
    return n*(start+doItExplicitely(n, diff, c))/2;
  }

  private static int doItRecursively(int n, int start, int diff) {
    if(n == 1){
      return start;      
    }else {
      return doItRecursively(n-1, start, diff)+ diff;
    }
  }

  private static int doItIterative(int start, int diff, int n) {
    int a1 = start;
    int d = diff;
    int an = a1;
    for(int i = 1; i <n; i++){
      an += d;
    }
    return an;
  }

  private static int doItExplicitely(int n, int diff, int c) {
    return n * diff - c;
  }  
}
