package uebung03.adapter;

import java.util.ArrayList;

public class ClassAdapter extends ArrayList<Bike> implements BikeStoreInterface{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void addBike(Bike b) {
    super.add(b);
  }

  @Override
  public Bike removeBike(int i) {
    return super.remove(i);
  }
}
