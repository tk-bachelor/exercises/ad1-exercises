/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon Apr 11 09:57:24 CEST 2016
 */

package uebung07.aufgabe02;

/**
 * Runtime exception thrown when one tries to perform an operation on an empty
 * queue.
 */

public class DequeEmptyException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public DequeEmptyException(String err) {
    super(err);
  }
} 
 
 
