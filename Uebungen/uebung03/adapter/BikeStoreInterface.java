package uebung03.adapter;

public interface BikeStoreInterface {

  public void addBike(Bike b);

  public Bike removeBike(int i);

  public void clear();

  public int size();
}
