/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 16 15:14:36 CEST 2016
 */

package uebung12.aufgabe02;

/**
 * A heap-based (array-implementation) Priority-Queue with fixed length.
 */
public class PriorityQueue<K extends Comparable<? super K>, V> {

  protected PQEntry<K, V>[] heapArray;

  /** Points to the last element in the heap. */
  protected int last = 0;

  public static class PQEntry<K extends Comparable<? super K>, V>
      implements Entry<K, V>, Comparable<PQEntry<K, V>> {

    protected K key;
    protected V value;

    protected PQEntry(K key, V value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public K getKey() {
      return key;
    }

    @Override
    public V getValue() {
      return value;
    }

    @Override
    public int compareTo(PQEntry<K, V> other) {
      return this.key.compareTo(other.key);
    }

    @Override
    public String toString() {
      return "(" + key + "," + value + ")";
    }

  }

  @SuppressWarnings("unchecked")
  public PriorityQueue(int maxSize) {
    heapArray = new PQEntry[maxSize + 1];
  }

  public Entry<K, V> insert(K key, V value) throws FullPriorityQueueException {
    if (last == heapArray.length - 1) {
      throw new FullPriorityQueueException();
    }

    PQEntry<K, V> entry = newEntry(key, value);
    last++;
    heapArray[last] = entry;

    // rearrange heap
    upheap(last);
    return entry;
  }

  protected void upheap(int position) {
    // position is not root
    if (position > 1) {
      int parentPos = position / 2;

      // its parent is greater
      if (heapArray[parentPos].compareTo(heapArray[position]) > 0) {
        // swap
        swap(parentPos, position);

        upheap(parentPos);
      }
    }
  }

  public Entry<K, V> min() {
    if (last == 0) {
      return null;
    }
    return heapArray[1];
  }

  public Entry<K, V> removeMin() {
    if (last == 0) {
      return null;
    }

    PQEntry<K, V> toBeRemoved = heapArray[1];

    heapArray[1] = heapArray[last];
    heapArray[last] = null;
    last--;

    downheap(1);

    return toBeRemoved;
  }

  /**
   * 
   * @param position
   */
  protected void downheap(int position) {
    int leftChildPos = position * 2;
    int rightChildPos = leftChildPos + 1;

    // no children --> nothing to do

    // only left child    
    if (last == leftChildPos) {
      swapNodes(position, leftChildPos);
    } else if (last >= rightChildPos) {
      // check if left position is null
      if (heapArray[leftChildPos] == null) {
        // only right child
        swapNodes(position, rightChildPos);
      }

      // check if right child is null
      else if (heapArray[rightChildPos] == null) {
        // only right child
        swapNodes(position, leftChildPos);
      }

      // both children
      else {
        // find the smallest one
        PQEntry<K, V> left = heapArray[leftChildPos];
        PQEntry<K, V> right = heapArray[rightChildPos];

        int smallestChildPos = left.compareTo(right) > 0 ? rightChildPos
            : leftChildPos;
        swapNodes(position, smallestChildPos);
      }
    }
  }

  private void swapNodes(int parentPos, int childPos) {
    PQEntry<K, V> parent = heapArray[parentPos];

    if (parent.compareTo(heapArray[childPos]) > 0) {
      swap(parentPos, childPos);

      // if child has children
      if (last >= childPos * 2) {
        downheap(childPos);
      }
    }
  }

  /**
   * Factory-Method for new entries.
   * 
   * @param key
   *          The key of the new entry.
   * @param value
   *          The value of the new entry.
   * @return The new created Entry.
   */
  protected PQEntry<K, V> newEntry(K key, V value) {
    return new PQEntry<K, V>(key, value);
  }

  public boolean isEmpty() {

    return last == 0;
  }

  public int size() {
    return last;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("[");
    for (int i = 0; i < heapArray.length; i++) {
      PQEntry<K, V> e = heapArray[i];
      if (e != null) {
        sb.append(e);
      } else {
        sb.append("null");
      }
      if (i < heapArray.length - 1) {
        sb.append(", ");
      }
    }
    sb.append("]");
    return sb.toString();
  }
  
  protected void swap(int parent, int child){
    PQEntry<K, V> entry = heapArray[parent];
    heapArray[parent] = heapArray[child];
    heapArray[child] = entry;
  }

  public void print() {
    System.out.println(toString());
  }

  /**
   * If GVS is in use: The actual state will be shown on GVS.
   */
  public void displayOnGVS() {
  }

}
