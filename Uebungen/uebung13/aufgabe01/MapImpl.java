/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 23 09:51:10 CEST 2016
 */

package uebung13.aufgabe01;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import uebung13.ml.aufgabe01.MapEntry;

public class MapImpl<K, V> implements Map<K, V> {

  private LinkedList<MapEntry<K, V>> list;
  
  
  public MapImpl() {
    list = new LinkedList<>();
  }

  public int size() {
    return list.size();
  }

  public boolean isEmpty() {
    return list.isEmpty();
  }

  public boolean containsKey(Object key) {
    for(MapEntry<K,V> entry : list){
      K currentKey = entry.getKey();
      if(currentKey==(K)key || currentKey != null && currentKey.equals(key)){
        return true;
      }
    }
    return false;
  }

  public boolean containsValue(Object value) {
    for(MapEntry<K,V> entry : list){
      V currentValue = entry.getValue();
      if(currentValue== (V) value || currentValue != null && currentValue.equals(value)){
        return true;
      }
    }
    return false;
  }

  public V get(Object key) {
    MapEntry<K, V> entry = find(key);
    if(entry != null){
      return entry.getValue();
    }
    return null;
  }
  
  private MapEntry<K, V> find(Object key){
    for(MapEntry<K,V> entry : list){
      K currentKey = entry.getKey();
      if(currentKey==(K)key || currentKey != null && currentKey.equals(key)){
        return entry;
      }
    }
    return null;
  }

  public V put(K key, V value) {
    return this.put(new MapEntry<K, V>(key, value));
  }

  private V put(MapEntry<K, V> entry) {
    MapEntry<K, V> found = find(entry.getKey());
    if(found == null){
      // add new
      list.add(entry);
      return null;
    }else{
      // replace value
      return found.setValue(entry.getValue());
    }
  }

  public V remove(Object key) {
    MapEntry<K, V> found = find(key);
    if(found == null)
      return null;
        
    list.remove(found);
    return found.getValue();
  }

  public void putAll(Map<? extends K, ? extends V> map) {
    for(java.util.Map.Entry<? extends K, ? extends V> entry : map.entrySet()){
      put(new MapEntry<K, V>(entry.getKey(), entry.getValue()));
    }
  }

  public void clear() {
    list.clear();
  }

  public Set<K> keySet() {
    Set<K> keys = new LinkedHashSet<>();
    for(Entry<K, V> entry : list){
      keys.add(entry.getKey());
    }
    return keys;
  }

  public Collection<V> values() {
    Collection<V> values = new ArrayList<V>();
    for(Entry<K, V> entry : list){
      values.add(entry.getValue());
    }
    return values;
  }

  public Set<Entry<K, V>> entrySet() {
    Set<Entry<K, V>> entries = new LinkedHashSet<>();
    for(Entry<K, V> entry : list){
      entries.add(entry);
    }
    return entries;
  }

  public void print() {
    System.out.println("Printing map (" + size() + " Entries): ");
    Iterator<MapEntry<K, V>> it = list.iterator();
    while (it.hasNext()) {
      MapEntry<K, V> e = it.next();
      System.out.format("  %3d: %s\n", e.getKey(), e.getValue());
    }
  }

  public static void main(String[] args) {
    
    MapImpl<Integer, String> map1 = new MapImpl<Integer, String>();
    map1.put(1, "one");
    map1.put(5, "five 1");
    map1.put(10, "ten");
    map1.put(5, "five 2");
    map1.put(54, "fifty-four");
    map1.print();

    MapImpl<Integer, String> map2 = new MapImpl<Integer, String>();
    map2.put(2, "two");
    map2.put(6, "six 1");
    map2.put(5, "five 3");
    map2.put(11, "eleven");
    map2.put(55, "fifty-five");
    map2.put(6, "six 2");
    map2.print();

    map1.putAll(map2);
    map1.print();
    
  }
}

/* Session-Log (SOLL -> Key=Value: Reihenfolge irrelevant):

Printing map (4 Entries): 
    1: one
    5: five 2
   10: ten
   54: fifty-four
Printing map (5 Entries): 
    2: two
    6: six 2
    5: five 3
   11: eleven
   55: fifty-five
Printing map (8 Entries): 
    1: one
    5: five 3
   10: ten
   54: fifty-four
    2: two
    6: six 2
   11: eleven
   55: fifty-five

*/

