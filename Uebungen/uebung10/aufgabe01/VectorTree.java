/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun May  1 18:31:42 CEST 2016
 */

package uebung10.aufgabe01;

import java.util.ArrayList;

public class VectorTree<T> implements TreeInterface<T> {

  private ArrayList<T> binaryTree;
  private int size;

  public VectorTree() {
    binaryTree = new ArrayList<T>();
    binaryTree.add(null); // 0
    binaryTree.add(null); // 1
  }

  public T root() {
    return binaryTree.get(1);
  }

  public void setRoot(T root) {
    // if the first element is not null
    if (root() != null) {
      try {
        removeLeftChild(root());
        removeRightChild(root());
      } catch (NoSuchNodeException e) {
        e.printStackTrace();
        System.exit(1);
      }
    }

    // set root as first element
    binaryTree.set(1, root);
    size = 1;
  }

  public T parent(T child) throws NoSuchNodeException {
    int pos = position(child);
    return binaryTree.get((pos - pos % 2) / 2);
  }

  public T leftChild(T parent) throws NoSuchNodeException {
    int pos = position(parent);
    if (binaryTree.size() >= posRightChild(pos)) {
      return binaryTree.get(posLeftChild(pos));
    } else {
      return null; // this object has no left child
    }
  }

  public T rightChild(T parent) throws NoSuchNodeException {
    int pos = position(parent);
    if (binaryTree.size() >= posRightChild(pos)) {
      return binaryTree.get(posRightChild(pos));
    } else {
      return null; // this object has no right child
    }
  }

  // branch - has children
  public boolean isInternal(T node) throws NoSuchNodeException {
    int pos = position(node);

    boolean isTreeBigEnough = binaryTree.size() > posLeftChild(pos);

    boolean hasLeft = isTreeBigEnough
        || binaryTree.get(posLeftChild(pos)) != null;

    boolean hasAnyChild = hasLeft || binaryTree.size() > posRightChild(pos)
        && binaryTree.get(posRightChild(pos)) != null;

    return hasAnyChild;
  }

  // end leaf = no children
  public boolean isExternal(T node) throws NoSuchNodeException {
    int pos = position(node);

    boolean isNoLeft = binaryTree.size() < posRightChild(pos) // is tree smaller than left child's position
        || binaryTree.get(posLeftChild(pos)) == null;

    boolean isNoRight = binaryTree.size() <= posRightChild(pos) // is tree smaller than right child's position
        || binaryTree.get(posRightChild(pos)) == null;

    return isNoLeft && isNoRight;
  }

  public boolean isRoot(T node) {
    return root().equals(node);
  }

  public void setRightChild(T parent, T child) throws NoSuchNodeException {
    this.removeRightChild(parent);
    int posRight = posRightChild(position(parent));
    assureSize(posRight);
    binaryTree.set(posRight, child);
    size++;
  }

  public void setLeftChild(T parent, T child) throws NoSuchNodeException {
    this.removeLeftChild(parent);
    int posLeft = posLeftChild(position(parent));
    assureSize(posLeft);
    binaryTree.set(posLeft, child);
    size++;
  }

  public void removeRightChild(T parent) throws NoSuchNodeException {
    int pos = position(parent);
    int rightPos = posRightChild(pos);

    if (binaryTree.size() >= rightPos) {
      T rightChild = binaryTree.get(rightPos);

      if (rightChild != null) {
        removeLeftChild(rightChild);
        removeRightChild(rightChild);

        binaryTree.set(rightPos, null);
        size--;
      }
    }
  }

  public void removeLeftChild(T parent) throws NoSuchNodeException {
    int pos = position(parent);
    int leftPos = posLeftChild(pos);

    if (binaryTree.size() >= leftPos) {
      T leftChild = binaryTree.get(leftPos);

      if (leftChild != null) {
        removeLeftChild(leftChild);
        removeRightChild(leftChild);

        binaryTree.set(leftPos, null);
        size--;
      }
    }
  }

  public int size() {
    return size;
  }

  private int posLeftChild(int pos) {
    return 2 * pos;
  }

  private int posRightChild(int pos) {
    return 2 * pos + 1;
  }

  private int position(T node) throws NoSuchNodeException {
    int index = binaryTree.indexOf(node);
    if (index == -1) {
      throw new NoSuchNodeException();
    }
    return index;
  }

  private void assureSize(int pos) {
    if (binaryTree.size() <= pos) {
      int addMore = pos + 1 - binaryTree.size();
      for (int i = 0; i < addMore; i++) {
        binaryTree.add(null);
      }
    }
  }

  public void printVector() {
    System.out.println(binaryTree);
  }

  public static void main(String[] args) throws NoSuchNodeException {

    // Hinweis: 
    // Beispiel ist aus Folien-Skript "Speicherverfahren für Bäume: Array basiert"

    VectorTree<Character> vt = new VectorTree<Character>();
    if (vt.size() != 0) {
      throw new Error("Bad size: " + vt.size() + " != 0");
    }
    if (vt.root() != null) {
      throw new Error("vt.root() != null");
    }

    System.out.println("\nSetting root with 'A':");
    Character a = 'A';
    vt.setRoot(a);
    vt.printVector();
    if (vt.size() != 1) {
      throw new Error("Bad size: " + vt.size() + " != 1");
    }
    if (!vt.isRoot(a)) {
      throw new Error("!vt.root(a)");
    }
    if (!vt.root().equals(a)) {
      throw new Error("!vt.root().equals(a) : " + vt.root());
    }
    if (!vt.isExternal(a)) {
      throw new Error("!vt.isExternal(a)");
    }
    if (vt.parent(a) != null) {
      throw new Error("vt.parent(a) != null");
    }

    System.out.println("\nSetting right child of 'A' with 'D':");
    Character d = 'D';
    vt.setRightChild(vt.root(), d);
    vt.printVector();
    if (vt.size() != 2) {
      throw new Error("Bad size: " + vt.size() + " != 2");
    }
    if (!vt.rightChild(vt.root()).equals(d)) {
      throw new Error(
          "!vt.rightChild(vt.root()).equals(d) : " + vt.rightChild(vt.root()));
    }
    if (!vt.isExternal(d)) {
      throw new Error("!vt.isExternal(d)");
    }
    if (!vt.isInternal(vt.root())) {
      throw new Error("!vt.isInternal(vt.root()");
    }
    if (!vt.parent(d).equals(a)) {
      throw new Error("!vt.parent(d).equals(a)");
    }

    System.out.println("\nSetting left child of 'A' with 'B':");
    Character b = 'B';
    vt.setLeftChild(vt.root(), b);
    vt.printVector();
    if (vt.size() != 3) {
      throw new Error("Bad size: " + vt.size() + " != 3");
    }

    System.out.println("\nSetting right child of 'B' with 'F':");
    Character f = 'F';
    vt.setRightChild(b, f);
    vt.printVector();

    System.out.println("\nSetting right child of 'F' with 'H':");
    Character h = 'H';
    vt.setRightChild(f, h);
    vt.printVector();

    System.out.println("\nSetting left child of 'F' with 'G':");
    Character g = 'G';
    vt.setLeftChild(f, g);
    vt.printVector();
    if (vt.size() != 6) {
      throw new Error("Bad size: " + vt.size() + " != 6");
    }
    if (!vt.isInternal(f)) {
      throw new Error("!vt.isInternal(f)");
    }
    if (!vt.isExternal(h)) {
      throw new Error("!vt.isExternal(h)");
    }
    if (!vt.rightChild(vt.rightChild(vt.leftChild(vt.root()))).equals(h)) {
      throw new Error(
          "!vt.rightChild(vt.rightChild(vt.leftChild(vt.root()))).equals(h)");
    }

    vt.removeLeftChild(b);
    if (vt.size() != 6) {
      throw new Error("Bad size: " + vt.size() + " != 6");
    }

    System.out.println("\nRemoving right child of 'B':");
    vt.removeRightChild(b);
    vt.printVector();
    if (vt.size() != 3) {
      throw new Error("Bad size: " + vt.size() + " != 3");
    }
    if (!vt.isExternal(b)) {
      throw new Error("!vt.isExternal(b)");
    }

    System.out.println("\nSetting right child of 'D' with 'J':");
    vt.setRightChild(d, 'J');
    vt.printVector();

    System.out.println("\nSetting right child of root 'A' with 'X':");
    vt.setRightChild(a, 'X');
    vt.printVector();
    if (vt.size() != 3) {
      throw new Error("Bad size: " + vt.size() + " != 3");
    }

    System.out.println("\nSetting root with 'Y':");
    vt.setRoot('Y');
    vt.printVector();
    if (vt.size() != 1) {
      throw new Error("Bad size: " + vt.size() + " != 1");
    }

  }
}

/* Session-Log:

Setting root with 'A':
[null, A]

Setting right child of 'A' with 'D':
[null, A, null, D]

Setting left child of 'A' with 'B':
[null, A, B, D]

Setting right child of 'B' with 'F':
[null, A, B, D, null, F]

Setting right child of 'F' with 'H':
[null, A, B, D, null, F, null, null, null, null, null, H]

Setting left child of 'F' with 'G':
[null, A, B, D, null, F, null, null, null, null, G, H]

Removing right child of 'B':
[null, A, B, D, null, null, null, null, null, null, null, null]

Setting right child of 'D' with 'J':
[null, A, B, D, null, null, null, J, null, null, null, null]

Setting right child of root 'A' with 'X':
[null, A, B, X, null, null, null, null, null, null, null, null]

Setting root with 'Y':
[null, Y, null, null, null, null, null, null, null, null, null, null]

*/
