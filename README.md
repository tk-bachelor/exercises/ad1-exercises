# AD1_Exercises
Algorithm and Data Structures Exercises

### Week 1 (23. Feb 2016) - Arithmetic Sequence
- Recursive, Iterative and Explicit

### Week 2 (01. Mar 2016) - Analysis Tool
- Recursive, Iterative and Explicit sequence number and sum
- Coin changer
- Big-O Notation

### Week 3 (08. Mar 2016) - O-Notation, Adapter Pattern
- Pair sum: O-Notation
- BikeStore Adapter Pattern

### Week 4 (15. Mar 2016) - Adapter Pattern, O-Notation
- Vector, Matrix Adapter Pattern
- O-Notation & Mathematical Induction

### Week 5 (22. Mar 2016) - Recursive
- Sum Square
- Search in a sorted array
- Fractal
- Tower of Hanoi

### Week 6 (05. Apr 2016) - Stack
- Sum odd numbers (loop vs recursive)
- Stack train
- Span efficient algorithm


### Week 7 (12. Apr 2016) - Doubly LinkedList and Deque 
- LinkedList (size, join two linked lists)
- Doubly LinkedList

### Week 8 (19. Apr 2016) - Stack and Queues
- Stack and Queue Implementation
- Permutation

### Week 9 (26. Apr 2016) - Backtracking
- Classic Backtracking (Labyrinth)
- Binary Tree

### Week 10 (03. May 2016) - Binary Tree
- Binary Tree with Vector
- Proof: Binary Tree

### Week 11 (10. May 2016) - Priority Queues
- Insertion Sort
- Priority Queue

### Week 12 (17. May 2016) - Heap
- Heap Operations
- Implementation: Priority Queue based on Heap
- Adaptable Priority Queue

### Week 13 (24. May 2016) - Map & Hash Table
- Map Implementation
- UID Generation
- Hash Collision