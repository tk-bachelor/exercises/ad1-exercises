/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun May  1 18:31:42 CEST 2016
 */

package uebung10.aufgabe01;

/**
 * Thrown if a reference to a node (parent or child) is given which is not part
 * of the tree.
 * 
 */
public class NoSuchNodeException extends Exception {

  private static final long serialVersionUID = 1L;

}
 
 
 
 
