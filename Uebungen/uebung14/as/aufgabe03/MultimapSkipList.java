/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 30 14:18:22 CEST 2016
 */

package uebung14.as.aufgabe03;

import java.util.*;

import uebung14.as.aufgabe02.SkipList;

/**
 * MultimapSkipList implements the Multimap-Interface. Therefore the
 * MultimapSkipList uses the Map-SkipList from ex 14, task 2 through aggregation:
 * -> Object-adapter
 */
public class MultimapSkipList<K extends Comparable<? super K>, V> implements
    Multimap<K, V> {


  public V find(K key) {
    // TODO Implement here...
    return null;
  }

  public Iterator<V> findAll(K key) {
    // TODO Implement here...
    return null;
  }

  public void insert(K key, V value) {
    // TODO Implement here...
  }

  public int size() {
    // TODO Implement here...
    return -1;
  }

  public int distinctKeys() {
    // TODO Implement here...
    return -1;
  }

  public void print() {
    // TODO Implement here...
  }

  public void remove(K key) {
    // TODO Implement here...
  }
  
}
 
 
 
