/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Feb 21 10:35:09 CET 2016
 */

package uebung01;

public class TextAnalyse {

  public static String s = "Das Studium an der HSR kann manchmal nerven, speziell beim Programmieren!“";
  public static char[] characs = { 'a', 'o', 'i', 'e', 'u' };

  public static void main(String[] args) {

    s = s.toLowerCase();

    for (int i = 0; i < characs.length; i++) {
      System.out.println(
          "Output: " + characs[i] + " = " + doIt(characs[i], s.length()));
    }
  }

  public static int doIt(char v, int len) {
    if (len > 1) {
      if (s.charAt(len - 1) == v)
        return doIt(v, len - 1) + 1;
      else
        return doIt(v, len - 1);
    } else {
      if (s.charAt(len - 1) == v)
        return 1;
      else
        return 0;
    }
  }

}
