/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 16 15:15:03 CEST 2016
 */

package uebung12.ml.aufgabe02;

public interface Entry<K, V> {

  K getKey();

  V getValue();
  
}
