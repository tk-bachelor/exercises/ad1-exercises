/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Thu May 12 11:20:35 CEST 2016
 */

package uebung11.ml.aufgabe03;

import java.util.ArrayList;

import uebung11.aufgabe02.Entry;

public class Frequency {

  public ArrayList<Entry<Long, Character>> countFrequency(String text) {
    long[] alfaCounts = new long[128];  // all ASCII-Characters
    for (int i = 0; i < text.length(); i++) {
      alfaCounts[text.charAt(i)]++;
    }
    ArrayList<Entry<Long, Character>> list = new ArrayList<Entry<Long, Character>>();
    for (int i = 0; i < alfaCounts.length; i++) {
      if (alfaCounts[i] > 0) {
        list.add(new Entry<Long, Character>(alfaCounts[i], (char)i));
      }
    }
    return list;
  }

  public static void main(String[] args) {
    Frequency freq = new Frequency();
    ArrayList<Entry<Long, Character>> list = freq
        .countFrequency("today's sunny weather - go out, have fun!");
    for (int i = 0; i < list.size(); i++) {
      Entry<Long, Character> entry = list.get(i);
      System.out.println("Element '" + entry.getValue()
          + "' has an occurence of " + entry.getKey());
    }
  }

}

/* Session-Log:

Element ' ' has an occurence of 7
Element '!' has an occurence of 1
Element ''' has an occurence of 1
Element ',' has an occurence of 1
Element '-' has an occurence of 1
Element 'a' has an occurence of 3
Element 'd' has an occurence of 1
Element 'e' has an occurence of 3
Element 'f' has an occurence of 1
Element 'g' has an occurence of 1
Element 'h' has an occurence of 2
Element 'n' has an occurence of 3
Element 'o' has an occurence of 3
Element 'r' has an occurence of 1
Element 's' has an occurence of 2
Element 't' has an occurence of 3
Element 'u' has an occurence of 3
Element 'v' has an occurence of 1
Element 'w' has an occurence of 1
Element 'y' has an occurence of 2

*/

 
 
 
 
