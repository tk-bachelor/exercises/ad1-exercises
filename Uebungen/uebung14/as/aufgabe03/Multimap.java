/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 30 14:18:22 CEST 2016
 */

package uebung14.as.aufgabe03;

import java.util.Iterator;

public interface Multimap<K extends Comparable<? super K>, V> {

  /**
   * Inserts a key/value pair into the multimap.
   */
  void insert(K key, V value);

  /**
   * Find the first value for a given key.
   * 
   * @return The value found, otherwise null.
   */
  V find(K key);

  /**
   * Find all values for a given key.
   * 
   * @return Iterator over all values. An empty iterator if the key does not
   *         exist.
   */
  Iterator<V> findAll(K key);

  /**
   * Remove all values for a given key.
   */
  void remove(K key);

  /**
   * Get the number of entries.
   * 
   * @return The size.
   */
  int size();

  /**
   * Get the number of keys (unique).
   * 
   * @return Number of unique keys.
   */
  int distinctKeys();

  /**
   * Pretty print the multimap.
   */
  void print();
}
 
 
 
