/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 30 14:18:22 CEST 2016
 */

package uebung14.ml.aufgabe03;

import static org.junit.Assert.*;

import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MultimapSkipListJUnitTest {

  MultimapSkipList<Integer, String> multimap;

  @Before
  public void setUp() {
    multimap = new MultimapSkipList<Integer, String>();
  }

  @Test
  public void test1Init() {
    assertEquals(0, multimap.size());
  }

  @Test
  public void test2Insert() {
    multimap.insert(1, "value_1");
    assertEquals(1, multimap.size());
    multimap.insert(1, "value_2");
    assertEquals(2, multimap.size());
    assertEquals(1, multimap.distinctKeys());
  }

  @Test
  public void test3Find() {
    assertNull(multimap.find(-1));
    test2Insert();
    assertNull(multimap.find(-1));
    Object obj = multimap.find(1);
    assertTrue(obj.equals("value_1") || obj.equals("value_2"));
  }

  @Test
  public void test4FindAll() {
    assertFalse(multimap.findAll(-1).hasNext());
    test2Insert();
    assertNull(multimap.find(-1));
    Iterator<?> it = multimap.findAll(1);
    assertTrue(it.hasNext());
    Object obj = it.next();
    assertTrue(obj.equals("value_1") || obj.equals("value_2"));
    assertTrue(it.hasNext());
    obj = it.next();
    assertTrue(obj.equals("value_1") || obj.equals("value_2"));
    assertFalse(it.hasNext());
  }

  @Test
  public void test5Remove() {
    test2Insert();
    multimap.remove(1);
    assertEquals(0, multimap.size());
  }
  
  @Test
  public void test6StressTest() {

    final int NUMBER_OF_TESTS = 1000;
    final int RANGE = 100;
    Random rand = new Random(1);
    Map<Integer, HashSet<String>> map = new HashMap<Integer, HashSet<String>>();

    for (int i = 0; i < NUMBER_OF_TESTS; i++) {
      int n = rand.nextInt() % RANGE;
      
      HashSet<String> mapSet = map.get(n);
      String str = null;
      if (mapSet == null) {
        mapSet = new HashSet<String>();
        map.put(n, mapSet);
        str = "String_0";
      }
      else {
        str = "String_"+(mapSet.size());
      }
      mapSet.add(str);
      multimap.insert(n, str);
      int sizeMap = 0;
      for (Entry<Integer, HashSet<String>> entry : map.entrySet()) {
        sizeMap += entry.getValue().size();
      }
      int sizeSkipList = multimap.size();
      assertEquals(sizeMap, sizeSkipList);
      
      for (int j = -RANGE; j <= RANGE; j++) {
        mapSet = map.get(j);
        Iterator<String> skipListIt = multimap.findAll(j);
        if (mapSet == null) {
          assertFalse(skipListIt.hasNext());
        } else {
          while (skipListIt.hasNext()) {
          str = skipListIt.next();
          assertTrue("mapSet does not contain: " + mapSet, 
              mapSet.contains(str));
          }
        }
      }
    }
    
    for (int i = -RANGE; i <= RANGE; i++) {
      map.remove(i);
      multimap.remove(i);
      int mapSize = map.entrySet().stream()
          .map(entry -> entry.getValue().size()).reduce(0, (a, b) -> a + b);
      int skipListSize = multimap.size();
      assertEquals(mapSize, skipListSize);
    }
    
  }

}
