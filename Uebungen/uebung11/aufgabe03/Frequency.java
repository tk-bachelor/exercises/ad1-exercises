/*
 * HSR - Uebungen Programmieren 2: Algorithmen & Datenstrukturen
 * Version: Sun May  8 16:24:03 CEST 2016
 */

package uebung11.aufgabe03;

import java.util.ArrayList;

import uebung11.aufgabe02.Entry;

public class Frequency {

  public ArrayList<Entry<Long, Character>> countFrequency(String text) {
    // TODO Implement here...
    return null;
  }

  public static void main(String[] args) {
    Frequency freq = new Frequency();
    ArrayList<Entry<Long, Character>> list = freq
        .countFrequency("today's sunny weather - go out, have fun!");
    for (int i = 0; i < list.size(); i++) {
      Entry<Long, Character> entry = list.get(i);
      System.out.println("Element '" + entry.getValue()
          + "' has an occurence of " + entry.getKey());
    }
  }

}

/* Session-Log (SOLL -> Reihenfolge irrelevant):

Element ' ' has an occurence of 7
Element '!' has an occurence of 1
Element ''' has an occurence of 1
Element ',' has an occurence of 1
Element '-' has an occurence of 1
Element 'a' has an occurence of 3
Element 'd' has an occurence of 1
Element 'e' has an occurence of 3
Element 'f' has an occurence of 1
Element 'g' has an occurence of 1
Element 'h' has an occurence of 2
Element 'n' has an occurence of 3
Element 'o' has an occurence of 3
Element 'r' has an occurence of 1
Element 's' has an occurence of 2
Element 't' has an occurence of 3
Element 'u' has an occurence of 3
Element 'v' has an occurence of 1
Element 'w' has an occurence of 1
Element 'y' has an occurence of 2

*/

 
 
 
 
