/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon Apr 11 09:59:08 CEST 2016
 */

package uebung07.ml.aufgabe02;

public class DequeImplementation<T> {

  Node<T> header, trailer;
  int size;

  public DequeImplementation() {
    header = new Node<T>();
    trailer = new Node<T>();
    header.setNext(trailer);
    trailer.setPrev(header);
    size = 0;
  }

  public void insertFirst(T element) {
    Node<T> second = header.getNext();
    Node<T> first = new Node<T>(element, header, second);
    second.setPrev(first);
    header.setNext(first);
    size++;
  }

  public T removeLast() throws DequeEmptyException {
    if (!isEmpty()) {
      Node<T> last = trailer.getPrev();
      T o = last.getElement();
      Node<T> secondtolast = last.getPrev();
      trailer.setPrev(secondtolast);
      secondtolast.setNext(trailer);
      size--;
      return o;
    } else {
      throw new DequeEmptyException("Deque is empty!");
    }
  }

  public T removeFirst() throws DequeEmptyException {
    if (!isEmpty()) {
      Node<T> first = header.getNext();
      T o = first.getElement();
      Node<T> second = first.getNext();
      header.setNext(second);
      second.setPrev(header);
      size--;
      return o;
    } else {
      throw new DequeEmptyException("Deque is empty!");
    }
  }

  public void insertLast(T element) {
    Node<T> node = new Node<T>(element, trailer.getPrev(), trailer);
    trailer.getPrev().setNext(node);
    trailer.setPrev(node);
    size++;
  }

  public T first() throws DequeEmptyException {
    if (!isEmpty()) {
      return header.getNext().getElement();
    } else {
      throw new DequeEmptyException("Deque is empty!");
    }
  }

  public T last() throws DequeEmptyException {
    if (!isEmpty()) {
      return trailer.getPrev().getElement();
    } else {
      throw new DequeEmptyException("Deque is empty!");
    }
  }

  public int size() {
    return size;
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public static void main(String[] args) {
    final int NR_OF_NODES = (int) 1e6;
    DequeImplementation<Integer> deque = new DequeImplementation<Integer>();
    Runtime.getRuntime().gc();
    long mem1 = Runtime.getRuntime().totalMemory()
        - Runtime.getRuntime().freeMemory();
    for (int i = 0; i < NR_OF_NODES / 2; i++) {
      // System.out.println("insertFirst(): "+i);
      deque.insertFirst(i);
    }
    for (int i = NR_OF_NODES / 2; i < NR_OF_NODES; i++) {
      // System.out.println("insertLast(): "+i);
      deque.insertLast(i);
    }
    System.out.println("first(): " + deque.first());
    System.out.println("last():  " + deque.last());
    Runtime.getRuntime().gc();
    long mem2 = Runtime.getRuntime().totalMemory()
        - Runtime.getRuntime().freeMemory();
    System.out.format("Memory usage: %,d   -> ca. %d  Bytes/Node\n", mem2
        - mem1, (mem2 - mem1) / NR_OF_NODES);
    Runtime.getRuntime().gc();
    for (int i = 0; i < NR_OF_NODES / 4; i++) {
      deque.removeFirst();
    }
    for (int i = 0; i < NR_OF_NODES / 4; i++) {
      deque.removeLast();
    }
    System.out.println("first(): " + deque.first());
    System.out.println("last():  " + deque.last());

    Runtime.getRuntime().gc();
    long mem3 = Runtime.getRuntime().totalMemory()
        - Runtime.getRuntime().freeMemory();
    System.out.format("Memory usage: %,d\n", mem3 - mem1);
    System.out.format("Size: %,d\n", deque.size());
  }

}

/* Session-Log:

first(): 499999
last():  999999
Memory usage: 39'999'984   -> ca. 39  Bytes/Node
first(): 249999
last():  749999
Memory usage: 20'036'024
Size: 500'000

*/ 
 
 
