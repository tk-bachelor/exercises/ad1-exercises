/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 30 14:18:22 CEST 2016
 */

package uebung14.ml.aufgabe03;

import java.util.Iterator;

/**
 * @author thabo Test class for the Multimap
 */
public class TestMultimap {

  public static void main(String[] args) {
    Multimap<String, String> multimap = new MultimapSkipList<String, String>();
    multimap.insert("essen", "eat");
    multimap.insert("essen", "manger");
    multimap.insert("essen", "mangiare");
    multimap.insert("essen", "magler");
    multimap.insert("essen", "taberu");
    multimap.insert("essen", "ginkhao");
    multimap.insert("trinken", "drink");
    multimap.insert("trinken", "boire");
    multimap.insert("trinken", "bere");
    multimap.insert("trinken", "bever");
    multimap.insert("trinken", "nomu");
    multimap.insert("trinken", "duum");
    multimap.insert("morgen", "moin");
    multimap.print();

    if (multimap.size() != 13) {
      System.err.println("Size not correct");
      System.exit(1);
    }
    if (multimap.distinctKeys() != 3) {
      System.err.println("Number of distinct keys not correct");
      System.exit(1);
    }
    Object a = multimap.find("essen");
    if (a == null) {
      System.err.println("Essen not found");
      System.exit(1);
    }
    a = multimap.find("trinken");
    if (a == null) {
      System.err.println("Trinken not found");
      System.exit(1);
    }
    a = multimap.find("morgen");
    if (a == null) {
      System.err.println("Morgen not found");
      System.exit(1);
    }

    Iterator<?> it = multimap.findAll("essen");
    int count = 0;
    System.out.println("Translations for essen: ");
    while (it.hasNext()) {
      System.out.println(it.next().toString());
      count++;
    }
    if (count != 6) {
      System.err.println("Amounts of translations not correct");
      System.exit(1);
    }
    System.out.println();

    it = multimap.findAll("trinken");
    count = 0;
    System.out.println("Translations for trinken: ");
    while (it.hasNext()) {
      System.out.println(it.next().toString());
      count++;
    }
    if (count != 6) {
      System.err.println("Amounts of translations not correct");
      System.exit(1);
    }
    System.out.println();

    it = multimap.findAll("morgen");
    count = 0;
    System.out.println("Translations for morgen: ");
    while (it.hasNext()) {
      System.out.println(it.next().toString());
      count++;
    }
    if (count != 1) {
      System.err.println("Amounts of translations not correct");
      System.exit(1);
    }
    System.out.println();

    multimap.remove("essen");
    if (multimap.size() != 7) {
      System.err.println("Size not correct");
      System.exit(1);
    }
    if (multimap.distinctKeys() != 2) {
      System.err.println("Number of distinct keys not correct");
      System.exit(1);
    }
    a = multimap.find("essen");
    if (a != null) {
      System.err.println("Essen not removed");
      System.exit(1);
    }
    multimap.print();
  }
}

/* Session-Log:

$ java -classpath Uebungen uebung14.ml.aufgabe03.TestMultimap
-------------------------------------------
Size: 3
Highest-Level: 5

essen     
essen     
essen     
essen     
essen           trinken 
essen   morgen  trinken 
--------------------------------------------------------------------------
Translations for essen: 
manger
magler
taberu
mangiare
ginkhao
eat

Translations for trinken: 
bever
boire
duum
bere
nomu
drink

Translations for morgen: 
moin

--------------------------------------------------------------------------
Size: 2
Highest-Level: 1

        trinken 
morgen  trinken 
-------------------------------------------

*/

 
 
 
