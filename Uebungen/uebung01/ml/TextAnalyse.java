/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Feb 28 18:26:29 CET 2016
 */

package uebung01.ml;

public class TextAnalyse {

  public static String s = "Test string to exercise....";
  public static char[] characs = { 'a', 'o', 'i', 'e', 'u' };

  public static void main(String[] args) {

    s = s.toLowerCase();

    int[] characsCounts = new int[characs.length];
    
    // method 1: doIt
    long nstart = System.nanoTime();
    doIt(characsCounts);
    long nstop = System.nanoTime();
    System.out.println("doIt: "+ (nstop - nstart));
    for (int i = 0; i < characs.length; i++) {
      System.out.println("Output: " + characs[i] + " = " + characsCounts[i]);
    }

  }
  
  public static void doIt(int[] characsCounts) {
    for (int j = 0; j < s.length(); j++) {
      for (int i = 0; i < characs.length; i++) {
        if (s.charAt(j) == characs[i]) {
          characsCounts[i]++;
          break;
        }
      }
    }
  }
  
  // more efficient code
  public static void doIt2(int[] characsCounts) {
    int[] alfaCounts = new int['z'];
    for (int j = 0; j < s.length(); j++) {
      alfaCounts[s.charAt(j)]++;
    }
    for (int i = 0; i < characs.length; i++) {
      characsCounts[i] = alfaCounts[characs[i]]; 
    }
  }
  
}

/* Session-Log:

Output: a = 0
Output: o = 1
Output: i = 2
Output: e = 4
Output: u = 0

*/
 
 
 
