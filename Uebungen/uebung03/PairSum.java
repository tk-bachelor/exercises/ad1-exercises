package uebung03;


public class PairSum {
  
  public static void main(String[] args) {
    int[] numbers = {47,73, 21, 17};
     
    long sum = totalPairSum(numbers);
    System.out.println(sum);
    
    int[] pairSum = pairSum(numbers);
    print(pairSum);
  }
  
  private static void print(int[] numbers){
    for(int i : numbers){
      System.out.print(i + " ");
    }
  }

  private static int[] pairSum(int[] numbers) {
    int totalPairs = numbers.length * (numbers.length - 1) / 2;
    int[] pairs = new int[totalPairs];
    int index = 0;
    
    for(int i = 0; i < numbers.length - 1 ; i++){
      for(int j = i+1; j < numbers.length; j++){
        pairs[index++] = numbers[i] + numbers[j];
      }
    }
    
    return pairs;
  }

  private static long totalPairSum(int[] numbers) {
    long sum = 0;
    
    // multiplier: maximum number of pairs for a single integer
    // O(n)
    int m = numbers.length-1;
    for(int i : numbers){
      sum += i*m;
    }
    return sum;
  }
}
