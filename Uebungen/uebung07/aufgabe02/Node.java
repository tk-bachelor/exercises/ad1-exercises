/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon Apr 11 09:57:24 CEST 2016
 */

package uebung07.aufgabe02;

/**
 * Template exercise 4 
 * Double linked list and DE-Queue (double ended queue)
 */
public class Node<T> {

  private T element;
  private Node<T> next, prev;

  Node() {
    this(null, null, null);
  }

  Node(T e, Node<T> p, Node<T> n) {
    element = e;
    next = n;
    prev = p;
  }

  void setElement(T newElem) {
    element = newElem;
  }

  void setNext(Node<T> newNext) {
    next = newNext;
  }

  void setPrev(Node<T> newPrev) {
    prev = newPrev;
  }

  T getElement() {
    return element;
  }

  Node<T> getNext() {
    return next;
  }

  Node<T> getPrev() {
    return prev;
  }
}
 
 
 
