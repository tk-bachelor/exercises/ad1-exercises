package uebung02;

public class BigO {

  // Exercise 4
  public void ex4(int n) {
    
    // 1 initialisation + n(addition + check)
    for (int i = 0; i < n; i++) {               // 1 + 2n
      // 1 init + 10 mil. (addition + check)
      for (int j = 0; j < 10_000_000; j++) {    // n(20mil + 1)
        // method call
        System.out.println(j);                  // 10mil n
      }
    }
    
    // Total: 3n + 30mil n
    // Worst case and Best case
    // => 10 million n
    
    
  }

}
