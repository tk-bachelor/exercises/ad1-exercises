/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon Apr 11 09:59:08 CEST 2016
 */

package uebung07.ml.aufgabe02;

/**
 * Runtime exception thrown when one tries to perform operation on an empty
 * queue.
 */

public class DequeEmptyException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public DequeEmptyException(String err) {
    super(err);
  }
} 
 
 
