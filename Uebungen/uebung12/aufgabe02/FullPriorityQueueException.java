/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 16 15:14:36 CEST 2016
 */

package uebung12.aufgabe02;


/**
 * Thrown at insert()-operation when PriorityQueue is already full.
 */
public class FullPriorityQueueException extends Exception {
  
  private static final long serialVersionUID = 1L;

  public FullPriorityQueueException() {
    super();
  }
}
