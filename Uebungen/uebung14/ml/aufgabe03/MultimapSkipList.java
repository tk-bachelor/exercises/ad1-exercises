/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 30 14:18:22 CEST 2016
 */

package uebung14.ml.aufgabe03;

import java.util.*;

import uebung14.ml.aufgabe02.SkipList;

/**
 * MultimapSkipList implements the Multimap-Interface. Therefore the
 * MultimapSkipList uses the Map-SkipList from ex 14, task 2 through aggregation:
 * -> Object-adapter
 */
public class MultimapSkipList<K extends Comparable<? super K>, V> implements
    Multimap<K, V> {

  public static final int MAX_SIZE = 100;
  /**
   * SkipList which implements a Map.
   */
  private SkipList<K, HashSet<V>> skipList;

  public MultimapSkipList() {
    skipList = new SkipList<K, HashSet<V>>(MAX_SIZE);
  }

  public V find(K key) {
    HashSet<V> set = skipList.get(key);
    if (set != null) {
      return set.iterator().next();
    }
    return null;
  }

  public Iterator<V> findAll(K key) {
    HashSet<V> set = skipList.get(key);
    if (set != null) {
      return set.iterator();
    }
    // Not found: return an empty iterator
    return new HashSet<V>().iterator(); 
  }

  public void insert(K key, V value) {
    HashSet<V> set = skipList.get(key);
    if (set == null) {
      set = new HashSet<V>();
      skipList.put(key, set);
    }
    set.add(value);
  }

  public int size() {
    SkipList.ListElement<K, HashSet<V>> aktElement = skipList.getHeader()
        .getForward()[0];
    int size = 0;
    while (aktElement.getForward()[0] != null) {
      HashSet<V> set = aktElement.getValue();
      size += set.size();
      aktElement = aktElement.getForward()[0];
    }
    return size;
  }

  public int distinctKeys() {
    return skipList.size();
  }

  public void print() {
    skipList.print();
  }

  public void remove(K key) {
    skipList.remove(key);
  }
}
 
 
 
