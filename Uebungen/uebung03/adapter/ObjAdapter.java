package uebung03.adapter;

import java.util.ArrayList;
import java.util.List;

public class ObjAdapter extends AbstractBikeStore{
  
  private List<Bike> store;
  
  
  public ObjAdapter() {
    store = new ArrayList<>();
  }

  @Override
  public void addBike(Bike b) {
    store.add(b);
  }

  @Override
  public Bike removeBike(int i) {
    return store.remove(i);
  }

  @Override
  public void clear() {
    store.clear();
  }

  @Override
  public int size() {
    return store.size();
  }

}
