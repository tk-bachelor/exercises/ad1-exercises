/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Mar 20 18:07:29 CET 2016
 */

package uebung05.aufgabe02;

import java.util.Scanner;

public class BinarySearch {

  public static int search(String[] array, String token, int start, int end) {
    int middle = (start+end) / 2;
    if(array[middle].equals(token)){
      return middle + 1;
    }else{
      // no string found
      if(start==end){
        return -1;
      }
      // search left
      if(token.compareTo(array[middle])<0){
        return search(array, token, start, middle);
      }else{
        // search right
        return search(array, token, middle+1, end);
      }
    }
  }

  public static void main(String[] args) {
    String[] array = new String[] { "Alge", "Ding", "Lang", "Politik", "Spiel",
        "Text", "Welt", "Zimmer" };

    System.out.println("Lang: " + search(array, "Lang", 0, array.length));
    System.out.println("Welt: " + search(array, "Welt", 0, array.length));
    System.out.println("Politik: " + search(array, "Politik", 0, array.length));

    System.out.print("Suchstring : ");
    Scanner scanner = new Scanner(System.in);
    String string = scanner.nextLine();
    scanner.close();
    System.out.println(search(array, string, 0, array.length));
    try {
      for (int i = 0; i < array.length; ++i) {
        if (search(array, array[i], 0, array.length) != i + 1) {

          throw new AssertionError("not working: "
              + search(array, array[i], 0, array.length) + "<>" + (i + 1));
        }
      }
    } catch (Exception ex) {
      throw new AssertionError();
    }
  }
}

/* Session-Log:
 
Lang: 3
Welt: 7
Politik: 4
Suchstring : Ding
2

*/

 
 
 
