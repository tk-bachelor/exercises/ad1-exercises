/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Apr 10 19:10:45 CEST 2016
 */

package uebung07.aufgabe01;

import java.util.Random;

/**
 * @title task 1 from exercise 4
 * @author tbeeler
 * 
 *         1a) see numNodes() 1b) see concatenate() 1c) see reverse*()
 */
public class LinkedList {

  /**
   * Solution task 1a. Recursive calculation of the numbers of nodes in the list
   * "head". Runtime O(n).
   * 
   * @param head
   *          Head of the list.
   * @return Number of nodes in the list.
   */
  public int numNodes(Node head) {
    if (head == null) {
      return 0;
    }
    return 1 + numNodes(head.getNext());
  }

  /**
   * Solution task 1b. Concatenate two lists l1 and l2 to a new list. l1 and l2
   * were not changed. If a change is allowed, the runtime can be improved to
   * O(|l1|). Runtime O(|l2|+|l1|) = O(n), whereas n is the number of elements
   * in the new list.
   * 
   * @param l1
   *          List 1.
   * @param l2
   *          List 2.
   * @return Concatenated list l1 and l2.
   */
  public Node concatenate(Node l1, Node l2) {
    Node l3 = null;

    if (l1 != null) {
      // append l1 and l2
      l3 = new Node(l1.getElement());
      l1 = l1.getNext();
      
      // append a new node to the tail
      Node tail = appendList(l3, l1);
      appendList(tail, l2);
     
    } else if (l2 != null) {
      // append l2 only
      l3 = new Node(l2.getElement());
      l2 = l2.getNext();
      appendList(l3, l2);
    }

    return l3;
  }

  /**
   * Solution task 1c. Invert the order of list 'head' by iteration. No new list
   * is built. The old head afterwards is the last node in the list. Runtime
   * O(n) (task).
   * 
   * @param head
   *          Head of the list.
   * @return New head.
   */
  public Node reverseByIteration(Node head) {
    Node reversedHead = null;
    Node previous = null;
    while(head != null){
      reversedHead = new Node(head.getElement());
      reversedHead.appendNode(previous);
      previous = reversedHead;
      head = head.getNext();
    }    
    return reversedHead;
  }

  /**
   * Solution task 1c. Invert the order of list 'head' by recursion. No new list
   * is built. The old head afterwards is the last node in the list. Uses
   * reverseByRecursionInternal(). Runtime O(n) (task).
   * 
   * @param head
   *          Head of the list.
   * @return New head.
   */
  public Node reverseByRecursion(Node head) {
    if(head == null){
      return null;
    }
    Node newHead = reverseByRecursionInternal(head);
    head.appendNode(null);
    return newHead;
  }

  /**
   * Internal recursive method which handles one node.
   * 
   * @param node
   *          The actual node.
   * @return New head.
   */
  private Node reverseByRecursionInternal(Node node) {
    if(node.getNext() == null){
      return node;
    }
    Node newHead = reverseByRecursionInternal(node.getNext());
    node.getNext().appendNode(node);
    return newHead;
  }

  /**
   * Solution task 1c with only one Method. Invert the order of list 'head' by
   * recursion. No new list is built. The old head afterwards is the last node
   * in the list. Runtime O(n) (task).
   * 
   * @param head
   *          Head of the list.
   * @return New head.
   */
  public Node reverseByRecursion2(Node head) {
    if(head == null || head.getNext() == null){
      return head;
    }
    Node next = head.getNext();
    head.appendNode(null);
    Node newHead = reverseByRecursion2(next);
    next.appendNode(head);
    return newHead;
  }

  /**
   * Print the elements of the list.
   * 
   * @param head
   *          Head of the list.
   */
  public void printList(Node head) {
    while (head != null) {
      System.out.print(head.getElement().toString());
      System.out.print(" ");
      head = head.getNext();
    }
    System.out.println();
  }

  /*
   * Add a copy of l2 to tail
   */
  private Node appendList(Node tail, Node l2) {
    while (l2 != null) {
      tail.appendNode(new Node(l2.getElement()));
      l2 = l2.getNext();
      tail = tail.getNext();
    }
    return tail;
  }

  /**
   * Tests the class exercise 1.
   */
  public static void main(String[] args) {
    System.out.println("=========================================");
    System.out.println("Exercise 7 - Task 1");
    System.out.println("Author: tbeeler@hsr.ch");
    final int L1 = 10;
    final int L2 = 15;
    LinkedList linkedList = new LinkedList();
    Random random = new Random(1);
    // create a List l1 with 10 elements
    Node head1 = new Node(Integer.valueOf((int) (random.nextDouble() * 100)));
    Node l1 = head1;
    for (int i = 1; i < L1; i++) {
      l1.appendNode(
          new Node(Integer.valueOf((int) (random.nextDouble() * 100))));
      l1 = l1.getNext();
    }
    System.out.println("List 1:");
    linkedList.printList(head1);
    System.out.println();
    // create a List l2 with 15 elements
    Node head2 = new Node(Integer.valueOf((int) (random.nextDouble() * 100)));
    Node l2 = head2;
    for (int i = 1; i < L2; i++) {
      l2.appendNode(
          new Node(Integer.valueOf((int) (random.nextDouble() * 100))));
      l2 = l2.getNext();
    }
    System.out.println("List 2:");
    linkedList.printList(head2);
    System.out.println();

    // Exercise 1 - count number of nodes    
    if (linkedList.numNodes(head1) != L1) {
      System.err.println("Node count does not match L1 ("
          + linkedList.numNodes(head1) + " != " + L1 + ")");
      System.exit(1);
    }
    if (linkedList.numNodes(head2) != L2) {
      System.err.println("Node count does not match L2 ("
          + linkedList.numNodes(head2) + " != " + L2 + ")");
      System.exit(1);
    }

    // Exercise 1b - concatenate lists
    Node head3 = linkedList.concatenate(head1, head2);
    System.out.println("List 3 = List 1 o List 2:");
    linkedList.printList(head3);
    System.out.println();
    if (linkedList.numNodes(head1) + linkedList.numNodes(head2) != linkedList
        .numNodes(head3)) {
      System.err.println("Node count does not match head3 ("
          + (linkedList.numNodes(head1) + linkedList.numNodes(head2)) + ")"
          + " != " + linkedList.numNodes(head3));
      System.exit(1);
    }
    Node head4 = linkedList.reverseByIteration(head3);
    System.out.println("List 4 = inverted List 3 by iteration:");
    linkedList.printList(head4);
    System.out.println();
    if (linkedList.numNodes(head1) + linkedList.numNodes(head2) != linkedList
        .numNodes(head4)) {
      System.err.println("Node count does not match head4 ("
          + (linkedList.numNodes(head1) + linkedList.numNodes(head2)) + ")"
          + " != " + linkedList.numNodes(head4));
      System.exit(1);
    }
    Node head5 = linkedList.reverseByRecursion(head4);
    System.out.println("List 5 = inverted List 4 by recursion:");
    linkedList.printList(head5);
    System.out.println();
    if (linkedList.numNodes(head1) + linkedList.numNodes(head2) != linkedList
        .numNodes(head5)) {
      System.err.println("Node count does not match head5 ("
          + (linkedList.numNodes(head1) + linkedList.numNodes(head2)) + ")"
          + " != " + linkedList.numNodes(head5));
      System.exit(1);
    }
    Node head6 = linkedList.reverseByRecursion2(head5);
    System.out.println("List 6 = inverted List 5 by recursion (version 2):");
    linkedList.printList(head6);
    System.out.println();
    if (linkedList.numNodes(head1) + linkedList.numNodes(head2) != linkedList
        .numNodes(head6)) {
      System.err.println("Node count does not match head6 ("
          + (linkedList.numNodes(head1) + linkedList.numNodes(head2)) + ")"
          + " != " + linkedList.numNodes(head6));
      System.exit(1);
    }

    System.out.println("\nDONE\n");
    System.out.println("=========================================");
  }
}

/**
 * @title Node
 * @author tbeeler
 * 
 *         Utility class. Nodes of a simple linked list.
 */
class Node {

  private Object element;
  private Node next;

  /**
   * Constructs a new unlinked node.
   * 
   * @param elem
   *          Object for the node.
   */
  public Node(Object elem) {
    element = elem;
    next = null;
  }

  /**
   * Adds the node next to this node.
   * 
   * @param next
   *          The next node.
   */
  public void appendNode(Node next) {
    this.next = next;
  }

  public Node getNext() {
    return next;
  }

  public Object getElement() {
    return element;
  }
}

/* Session-Log (SOLL):

=========================================
Exercise 4 - Task 1
Author: tbeeler@hsr.ch
List 1:
73 41 20 33 96 0 96 93 94 93 

List 2:
39 34 29 50 11 77 65 15 37 13 69 80 0 52 74 

List 3 = List 1 o List 2:
73 41 20 33 96 0 96 93 94 93 39 34 29 50 11 77 65 15 37 13 69 80 0 52 74 

List 4 = inverted List 3 by iteration:
74 52 0 80 69 13 37 15 65 77 11 50 29 34 39 93 94 93 96 0 96 33 20 41 73 

List 5 = inverted List 4 by recursion:
73 41 20 33 96 0 96 93 94 93 39 34 29 50 11 77 65 15 37 13 69 80 0 52 74 

List 6 = inverted List 5 by recursion (version 2):
74 52 0 80 69 13 37 15 65 77 11 50 29 34 39 93 94 93 96 0 96 33 20 41 73 


DONE

=========================================

*/
