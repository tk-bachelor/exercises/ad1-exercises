/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Feb 28 18:34:58 CET 2016
 */

package uebung02;

import java.util.Scanner;

public class Greedy {

  private static int[] bc = { 200, 100, 50, 20, 10, 5, 2, 1 };

  public static void main(String[] args) {
    System.out.print("Amount-in-CHF = ");
    Scanner scanner = new Scanner(System.in);
    greedy(scanner.nextInt());
    scanner.close();
  }

  public static void greedy(int amount) {
    if (amount > 999) {
      throw new IllegalArgumentException(
          "Sorry, there's no change for this amount");
    }

    int[] counts = new int[bc.length];

    for (int i = 0; i < bc.length; i++) {
      while (bc[i] <= amount) {
        amount -= bc[i];
        counts[i]++;
      }

      if (amount == 0) {
        break;
      }
    }

    // print result
    for (int i = 0; i < counts.length; i++) {
      if (counts[i] != 0) {
        String s = String.format("%d CHF %s", bc[i],
            (bc[i] < 10 ? "coin" : "bill"));
        System.out.println(s);
      }
    }
  }
}

/* Session-Log:

Amount-in-CHF = 28
20 CHF bill
5 CHF coin
2 CHF coin
1 CHF coin

*/
