/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 26 11:48:59 CEST 2016
 */

package uebung13.ml.aufgabe01;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@SuppressWarnings("unused")
public class MapImpl<K, V> implements Map<K, V> {

  private LinkedList<MapEntry<K, V>> list = new LinkedList<>();

  public int size() {
    return list.size();
  }

  public boolean isEmpty() {
    return list.isEmpty();
  }

  
  // containsKey():

  public boolean containsKey(Object key) {
    MapEntry<K, V> entry = find(key);
    return (entry != null);
  }

  private MapEntry<K, V> find(Object key) {
    @SuppressWarnings("unchecked")
    MapEntry<K, V> sentinel = new MapEntry<K, V>((K)key, null);
    list.addLast(sentinel); 
    Iterator<MapEntry<K, V>> it = list.iterator();
    while (true) {
      MapEntry<K, V> e = it.next();
      K thisKey = e.getKey();
      if (thisKey == key || thisKey != null && thisKey.equals(key)) {
        list.removeLast();  // remove sentinel
        if (e == sentinel) {
          return null;
        } else {
          return e;
        }
      }
    }
  }

  //  -------------------------------------------
  //  for each:
  //  -------------------------------------------
  //  public boolean containsKey(Object key) {
  //    for (MapEntry<K, V> entry : list) {
  //      K thisKey = entry.getKey();
  //      if (thisKey == key || thisKey != null && thisKey.equals(key)) {
  //        return true;
  //      }
  //
  //    }
  //    return false;
  //  }
  //
  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public boolean containsKey(Object key) {
  //    return list.stream().anyMatch(e -> compareKeys(key, e));
  //  }

  
  // containsValue():

  public boolean containsValue(Object value) {
    Iterator<MapEntry<K, V>> it = list.iterator();
    while (it.hasNext()) {
      V thisValue = it.next().getValue();
      if (thisValue == value || thisValue != null && thisValue.equals(value)) {
        return true;
      }
    }
    return false;
  }

  //  -------------------------------------------
  //  for each:
  //  -------------------------------------------
  //  public boolean containsValue(Object value) {
  //    for (MapEntry<K, V> entry : list) {
  //      V thisValue = entry.getValue();
  //      if (thisValue == value || thisValue != null && thisValue.equals(value)) {
  //        return true;
  //      }
  //
  //    }
  //    return false;
  //  }
  //
  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public boolean containsValue(Object value) {
  //    return list.stream().anyMatch(e -> compareValues(value, e));
  //  }

  
  // get():

  public V get(Object key) {
    MapEntry<K, V> entry = find(key);
    if (entry != null) {
      return entry.getValue();
    } else {
      return null;
    }
  }
  
  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public V get(Object key) {
  //    MapEntry<K, V> findFirst = findEntry(key);
  //    if (findFirst != null) {
  //      return findFirst.getValue();
  //    }
  //    return null;
  //  }
  //
  //  private MapEntry<K, V> findEntry(Object key) {
  //    Optional<MapEntry<K, V>> findFirst = list.stream()
  //        .filter(e -> compareKeys(key, e)).findFirst();
  //    if (findFirst.isPresent()) {
  //      return findFirst.get();
  //    }
  //    return null;
  //  }

  
  // put():

  public V put(K key, V value) {
    return put(new MapEntry<K, V>(key, value));
  }

  private V put(MapEntry<K, V> entry) {
    MapEntry<K, V> foundEntry = find(entry.getKey());
    if (foundEntry != null) {
      return foundEntry.setValue(entry.getValue());
    } else {
      list.add(entry);
      return null;
    }
  }
  
  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public V put(K key, V value) {
  //    MapEntry<K, V> findFirst = findEntry(key);
  //    if (findFirst != null) {
  //      V oldValue = findFirst.getValue();
  //      findFirst.setValue(value);
  //      return oldValue;
  //    }
  //    list.add(new MapEntry<>(key, value));
  //    return null;
  //  }

  
  // remove():

  public V remove(Object key) {
    MapEntry<K, V> entry = find(key);
    if (entry != null) {
      list.remove(entry);
      return entry.getValue();
    } else {
      return null;
    }
  }
  
  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public V remove(Object key) {
  //    MapEntry<K, V> findFirst = findEntry(key);
  //    if (findFirst != null) {
  //      list.remove(findFirst);
  //      return findFirst.getValue();
  //    }
  //    return null;
  //  }

  
  // putAll():

  public void putAll(Map<? extends K, ? extends V> map) {
    Entry<? extends K, ? extends V> e;
    Iterator<? extends Entry<? extends K, ? extends V>> it = map.entrySet()
        .iterator();
    while (it.hasNext()) {
      e = it.next();
      put(new MapEntry<K, V>(e.getKey(), e.getValue()));
    }
  }

  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public void putAll(Map<? extends K, ? extends V> map) {
  //    map.entrySet().stream().forEach(e -> put(e.getKey(), e.getValue()));
  //  }
  

  public void clear() {
    list = new LinkedList<>();
  }

  
  // keySet():

  public Set<K> keySet() {
    Set<K> s = new LinkedHashSet<K>();
    Iterator<MapEntry<K, V>> it = list.iterator();
    while (it.hasNext()) {
      s.add((it.next()).getKey());
    }
    return s;
  }

  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public Set<K> keySet() {
  //    return list.stream().map(MapEntry::getKey).collect(Collectors.toSet());
  //  }

  
  // values():

  public Collection<V> values() {
    Collection<V> s = new LinkedList<V>();
    Iterator<MapEntry<K, V>> it = list.iterator();
    while (it.hasNext()) {
      s.add(it.next().getValue());
    }
    return s;
  }

  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public Collection<V> values() {
  //    return list.stream().map(MapEntry::getValue).collect(Collectors.toSet());
  //  }

  
  // entrySet():

  public Set<Entry<K, V>> entrySet() {
    Set<Entry<K, V>> s = new LinkedHashSet<Entry<K, V>>();
    Iterator<MapEntry<K, V>> it = list.iterator();
    while (it.hasNext()) {
      s.add(it.next());
    }
    return s;
  }

  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public Set<Entry<K, V>> entrySet() {
  //    return list.stream().collect(Collectors.toSet());
  //  }

  
  // print():

  public void print() {
    System.out.println("Printing map (" + size() + " Entries): ");
    Iterator<MapEntry<K, V>> it = list.iterator();
    while (it.hasNext()) {
      MapEntry<K, V> e = it.next();
      System.out.format("  %3d: %s\n", e.getKey(), e.getValue());
    }
  }

  //  -------------------------------------------
  //  streams:
  //  -------------------------------------------
  //  public void print() {
  //    list.stream().forEach(System.out::println);
  //  }

  
  private boolean compareKeys(Object key, MapEntry<K, V> entry) {
    return compare(key, entry.getKey());
  }

  private boolean compareValues(Object value, MapEntry<K, V> entry) {
    return compare(value, entry.getValue());
  }

  private boolean compare(Object obj1, Object obj2) {
    if (obj1 != null) {
      return obj1.equals(obj2);
    } else {
      return obj2 == null;
    }
  }

  public static void main(String[] args) {

    MapImpl<Integer, String> map1 = new MapImpl<Integer, String>();
    map1.put(1, "one");
    map1.put(5, "five 1");
    map1.put(10, "ten");
    map1.put(5, "five 2");
    map1.put(54, "fifty-four");
    map1.print();

    MapImpl<Integer, String> map2 = new MapImpl<Integer, String>();
    map2.put(2, "two");
    map2.put(6, "six 1");
    map2.put(5, "five 3");
    map2.put(11, "eleven");
    map2.put(55, "fifty-five");
    map2.put(6, "six 2");
    map2.print();

    map1.putAll(map2);
    map1.print();

  }
}

/* Session-Log (SOLL -> Key=Value: Reihenfolge irrelevant):

Printing map (4 Entries): 
    1: one
    5: five 2
   10: ten
   54: fifty-four
Printing map (5 Entries): 
    2: two
    6: six 2
    5: five 3
   11: eleven
   55: fifty-five
Printing map (8 Entries): 
    1: one
    5: five 3
   10: ten
   54: fifty-four
    2: two
    6: six 2
   11: eleven
   55: fifty-five

*/

