/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Fri Mar 11 17:23:10 CET 2016
 */

package uebung04.adapter;

public class VektorImpl implements Vektor {

  private MatrixImpl matrix;

  public VektorImpl(int i) {
    matrix = new MatrixImpl(i, 1);
  }

  public int get_size() {
    return matrix.get_sizex();
  }

  public double get(int i) {
    return matrix.get(i, 0);
  }

  public void set(int i, double val) {
    matrix.set(i, 0, val);
  }

  public double mult(Vektor right) {
    Matrix m = as_matrix().mult(right.as_matrix());
    double val = 0;
    for(int i = 0; i < m.get_sizex(); i++){
      val += Math.pow(m.get(0, i), 2);
    }
    return Math.sqrt(val);
  }

  public Vektor mult(Matrix left) {
    Matrix m = left.mult(matrix);
    Vektor v = new VektorImpl(m.get_sizex());
    v.setMatrix(m);
    return v;
  }

  public void transpose() {
    matrix.transpose();
  }

  public Matrix as_matrix() {
    return matrix;
  }

  public Vektor copy() {
    Vektor v2 = new VektorImpl(get_size());
    for(int i = 0; i < matrix.get_sizex(); i++){
      v2.set(i, matrix.get(i, 0));
    }
    return v2;
  }

  public void print() {
    matrix.print();
  }

  @Override
  public void setMatrix(Matrix m) {
    this.matrix = (MatrixImpl) m;
  }
}
 
 
