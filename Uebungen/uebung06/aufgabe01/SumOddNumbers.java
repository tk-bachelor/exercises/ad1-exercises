/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Apr  3 10:36:26 CEST 2016
 */

package uebung06.aufgabe01;

public class SumOddNumbers {

  /**
   * Calculates the sum of all odd numbers from 1 to 'n' with iteration.
   * 
   * Precondition: 'n' is an odd number.
   * (Precondition: see also http://en.wikipedia.org/wiki/Precondition)
   * 
   * @param n
   *          The upper limit of the summation.
   * @return The sum of all odd numbers from 1 to 'n'.
   */
  public int perLoop(int n) {
    int r = 0;
    for(int i = 1; i <= n; i +=2){
      if(i % 2 == 1){
        r += i;
      }
    }
    return r;
  }

  /**
   * Calculates the sum of all odd numbers from 1 to 'n' with recursion.
   * 
   * Precondition: 'n' is an odd number.
   * (Precondition: see also http://en.wikipedia.org/wiki/Precondition)
   * 
   * @param n
   *          The upper limit of the summation.
   * @return The sum of all odd numbers from 1 to 'n'.
   */
  public int perRecursion(int n) {
    if(n == 1){
      return 1;
    }else{
      return n + perRecursion(n - 2);
    }
  }

  public static void main(String[] args) {
    int n = 100;
    
    // ensure (next) odd number (is a precondition of perLoop() and perRecursion())
    n = n + (n % 2 - 1);
    
    SumOddNumbers sum = new SumOddNumbers();
    System.out.println("Iteration: " + sum.perLoop(n));
    System.out.println("Recursion: " + sum.perRecursion(n));
  }

}

/* Session-Log:

Iteration: 2500
Recursion: 2500

*/

 
 
 
