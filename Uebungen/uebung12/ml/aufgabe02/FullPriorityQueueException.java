/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 16 15:15:03 CEST 2016
 */

package uebung12.ml.aufgabe02;


/**
 * Thrown at insert()-operation when PriorityQueue is already full.
 */
public class FullPriorityQueueException extends Exception {
  
  private static final long serialVersionUID = 1L;

  public FullPriorityQueueException() {
    super();
  }
}
