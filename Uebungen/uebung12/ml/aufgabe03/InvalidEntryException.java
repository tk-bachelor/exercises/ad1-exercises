/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 16 15:15:18 CEST 2016
 */

package uebung12.ml.aufgabe03;


/**
 * Thrown when an entry is discovered to be invalid.
 */
public class InvalidEntryException extends Exception {
  
  private static final long serialVersionUID = 1L;

  public InvalidEntryException(String message) {
    super(message);
  }
}
