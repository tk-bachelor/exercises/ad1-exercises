/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Apr 17 20:05:29 CEST 2016
 */

package uebung08.aufgabe01;

/**
 * Runtime exception thrown when one tries to perform operation top or pop on an
 * empty stack.
 */

public class EmptyStackException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public EmptyStackException(String err) {
    super(err);
  }
}