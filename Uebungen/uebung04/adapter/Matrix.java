/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Fri Mar 11 17:23:10 CET 2016
 */

package uebung04.adapter;

public interface Matrix {

  int get_sizex();

  int get_sizey();

  double get(int x, int y);

  void set(int x, int y, double val);

  void transpose();

  Matrix mult(Matrix right);

  Matrix copy();

  void print();
}
 
 
