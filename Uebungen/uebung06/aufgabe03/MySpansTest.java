/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Apr  3 10:32:29 CEST 2016
 */

package uebung06.aufgabe03;

import java.util.Arrays;
import java.util.Stack;

class MySpanTest {

  public static void main(String[] args) {
    if (args.length == 0) {
      System.err.println("ERROR: bad number of arguments!");
      usage();
    }
    
    int[] x1 = {6, 3, 4, 5, 2};
    int[] x2 = {6, 3, 4, 1, 2, 3, 5, 4};
    
    // Span1 Quadratic
    System.out.println("Quadratic");
    System.out.println("===================");
    print(x1, span1(x1, x1.length));
    print(x2, span1(x2, x2.length));  
    
    System.out.println("\n");
    
    // Span2 Linear
    System.out.println("Linear");
    System.out.println("===================");
    print(x2, span2(x1, x1.length));
    print(x2, span2(x2, x2.length));
    
    // Runtime measurement both
        
  }  

  private static int[] span1(int[] array, int n) {
    int[] spans = new int[n];
    for(int i=0; i < n ; i++){
      int s = 1;
      while(s <= i && array[i-s] <=array[i]){
        s++;
      }
      spans[i]=s;
    }
    return spans;    
  }

  private static int[] span2(int[] array, int n) {
    int[] spans = new int[n];
    Stack<Integer> stack = new Stack<Integer>();        
    for(int i = 0; i<n; i++){
      while(!stack.isEmpty() && array[stack.peek()] <= array[i]){
        stack.pop();
      }
      spans[i] = stack.isEmpty() ? i+1 : i - stack.peek();
      stack.push(i);
    }
    return spans;
  }
  
  private static void print(int[] array, int[] span){
    System.out.println("Input: " + Arrays.toString(array));
    System.out.println("Result: " + Arrays.toString(span));
  }

  private static void usage() {
    System.err.println("usage: uebung06.as.aufgabe03.SpansTest [-t] algorithm-number");
    System.err.println("       -t              :  additional tracing.");
    System.err.println("       algorithm-number:  1|2|12");
    System.err.println("          1: quadratic");
    System.err.println("          2: linear");
    System.err.println("         12: runtime-measurement with both");
  }

} // end of class SpansTest


/* Session-Logi (SOLL):

$ java uebung06.as.aufgabe03.SpansTest 1
Input  x1: 6 3 4 5 2
Result s1: 1 1 2 3 1

Input  x2: 6 3 4 1 2 3 5 4
Result s2: 1 1 2 1 2 3 6 1



$ java uebung06.as.aufgabe03.SpansTest 2
Input  x1: 6 3 4 5 2
Result s1: 1 1 2 3 1

Input  x2: 6 3 4 1 2 3 5 4
Result s2: 1 1 2 1 2 3 6 1



$ java -Xint -Xms10m -Xmx10m muebung06.as.aufgabe03.SpansTest 12
Algorithm: QUADRATIC   Size:  1'000    Time:    11.8 ms    Ratio to last: 0.0
Algorithm: LINEAR      Size:  1'000    Time:     1.8 ms    Ratio to last: 0.0
Algorithm: QUADRATIC   Size:  2'000    Time:    47.8 ms    Ratio to last: 4.0
Algorithm: LINEAR      Size:  2'000    Time:     3.5 ms    Ratio to last: 1.9
Algorithm: QUADRATIC   Size:  4'000    Time:   190.1 ms    Ratio to last: 4.0
Algorithm: LINEAR      Size:  4'000    Time:     6.6 ms    Ratio to last: 1.9
Algorithm: QUADRATIC   Size:  8'000    Time:   771.9 ms    Ratio to last: 4.1
Algorithm: LINEAR      Size:  8'000    Time:    12.6 ms    Ratio to last: 1.9
Algorithm: QUADRATIC   Size: 16'000    Time: 3'159.2 ms    Ratio to last: 4.1
Algorithm: LINEAR      Size: 16'000    Time:    25.4 ms    Ratio to last: 2.0


$ java uebung06.as.aufgabe03.SpansTest -t 2
Input  x1: 6 3 4 5 2
i : 0
S: 1 0 0 0 0
A: 0
------------------
i : 1
S: 1 1 0 0 0
A: 1 0
------------------
i : 2
S: 1 1 2 0 0
A: 2 0
------------------
i : 3
S: 1 1 2 3 0
A: 3 0
------------------
i : 4
S: 1 1 2 3 1
A: 4 3 0
------------------
Result s1: 1 1 2 3 1

Input  x2: 6 3 4 1 2 3 5 4
i : 0
S: 1 0 0 0 0 0 0 0
A: 0
------------------
i : 1
S: 1 1 0 0 0 0 0 0
A: 1 0
------------------
i : 2
S: 1 1 2 0 0 0 0 0
A: 2 0
------------------
i : 3
S: 1 1 2 1 0 0 0 0
A: 3 2 0
------------------
i : 4
S: 1 1 2 1 2 0 0 0
A: 4 2 0
------------------
i : 5
S: 1 1 2 1 2 3 0 0
A: 5 2 0
------------------
i : 6
S: 1 1 2 1 2 3 6 0
A: 6 0
------------------
i : 7
S: 1 1 2 1 2 3 6 1
A: 7 6 0
------------------
Result s2: 1 1 2 1 2 3 6 1

*/
