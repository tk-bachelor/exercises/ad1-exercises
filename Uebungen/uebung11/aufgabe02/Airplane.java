/*
 * HSR - Uebungen Programmieren 2: Algorithmen & Datenstrukturen
 * Version: Sun May  8 16:24:02 CEST 2016
 */

package uebung11.aufgabe02;

public class Airplane {  

  private String departureAirport;
  private long quantityOfPetrol;

  public Airplane(String departureAirport, long quantityOfPetrol) {
    this.departureAirport = departureAirport;
    this.quantityOfPetrol = quantityOfPetrol;
  }

  public String getDepartureAirport() {
    return departureAirport;
  }

  public long getQuantityOfPetrol() {
    return quantityOfPetrol;
  }
}
 
 
 
 
