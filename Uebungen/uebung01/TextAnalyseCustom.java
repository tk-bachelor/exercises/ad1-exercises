/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Feb 21 10:38:16 CET 2016
 */

package uebung01;

public class TextAnalyseCustom {

  public static String s = "Test string to exercise....";
  public static char[] characs = { 'a', 'o', 'i', 'e', 'u' };

  public static void main(String[] args) {

    s = s.toLowerCase();

    int[] characsCounts = new int[characs.length];
    doIt(characsCounts);
    for (int i = 0; i < characs.length; i++) {
      System.out.println("Output: " + characs[i] + " = " + characsCounts[i]);
    }
  }
  
  public static void doIt(int[] characsCounts) {
    // loop through string text
    for(int i=0; i < s.length(); i++){
      
      // loop through vowels
      for(int c=0; c < characs.length; c++){
        if(s.charAt(i)==characs[c]){
          characsCounts[c]++;
          break;
        }        
      }
    }
    
  }
  
}

/* Session-Log (SOLL):

Output: a = 0
Output: o = 1
Output: i = 2
Output: e = 4
Output: u = 0

*/
 
 
 
