/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Mar  6 16:50:20 CET 2016
 */

package uebung03.adapter;

public abstract class AbstractBikeStore {

  public abstract void addBike(Bike b);

  public abstract Bike removeBike(int i);

  public abstract void clear();

  public abstract int size();
}
 
 
