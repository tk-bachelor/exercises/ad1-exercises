/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sat Apr 23 21:19:54 CEST 2016
 */

package uebung09.aufgabe01;

import com.sun.xml.internal.ws.wsdl.writer.document.OpenAtts;

public class Walker {

  enum Direction {

    NORTH(0, -1), WEST(-1, 0), EAST(1, 0), SOUTH(0, 1);

    private int offsetX;
    private int offsetY;

    Direction(int offsetX, int offsetY) {
      this.offsetX = offsetX;
      this.offsetY = offsetY;
    }

    int getOffsetX() {
      return offsetX;
    }

    int getOffsetY() {
      return offsetY;
    }

  }

  /**
   * The Labyrinth where the Walker is.
   */
  private Labyrinth lab;

  public Walker() {
    lab = new Labyrinth();
  }

  /**
   * Recursive method, which is called for every position to check.
   * 
   * @param x
   *          X-Position to check.
   * @param y
   *          Y-Position to check.
   * @return True if the destination is found, else false.
   */
  public boolean walk(int x, int y) {    
    // invalid position
    try {
      PositionState posState = lab.getField(x, y);
      
      // check if the destination is reached
      if (lab.getEndRow() == x && lab.getEndCol() == y) {
        return true;
      }
      // wall
      else if(posState == PositionState.WALL || posState == PositionState.WALKED){
        return false;
      }
      else if (lab.getEndRow() == x && lab.getEndCol() == y) {
        return true;
      } else {
        lab.setField(x, y, PositionState.CURRENT_POSITION);
        printLabyrinth();

        lab.setField(x, y, PositionState.WALKED);
        boolean result = walk(x + Direction.NORTH.offsetX, y + Direction.NORTH.offsetY) 
            || walk(x + Direction.WEST.offsetX, y + Direction.WEST.offsetY)
            || walk(x + Direction.SOUTH.offsetX, y + Direction.SOUTH.offsetY)
            || walk(x + Direction.EAST.offsetX, y + Direction.EAST.offsetY);
        lab.setField(x, y, PositionState.BACKTRACKED);
        return result;
      }
    } catch (Exception ex) {
      return false;
    }
  }

  private void printLabyrinth() {
//    System.out.print(
//        "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    System.out.println("\n");
    lab.print();
  }

  private void sleep(int ms) {
    try {
      Thread.sleep(ms);
    } catch (InterruptedException ex) {
    }
  }

  public static void main(String[] args) {
    Walker walker = new Walker();
    // start walking at the start position
    if (walker.walk(walker.lab.getStartCol(), walker.lab.getStartRow())) {
      System.out.println("Finally found a way out!");
    } else {
      System.out.println("Desperate search ended unsuccessful...");
    }
  }
}

/* Session-Log (SOLL):

 # # # # # # # # # # # # #
 # o # . . # . . . . . . #
 # o # . # # . # . # . # #
 # o o . # . . # . # . . #
 # # o # # # # # . # # # #
 # . o o o o o o o o o . #
 # . # # # o o # # # o # #
 # . . . # o o o o o o   #
 # # # # # o # # # # #   #
 # . . . . o # . #   #   #
 # . # # # o # . # X # # #
 # . # . # o o o # o #   #
 # . . . # # # o o o     #
 # # # # # # # # # # # # #
Evaluating new possible solution (9/10)
Finally found a way out!

*/
