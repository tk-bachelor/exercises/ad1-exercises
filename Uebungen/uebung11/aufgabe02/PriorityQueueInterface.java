/*
 * HSR - Uebungen Programmieren 2: Algorithmen & Datenstrukturen
 * Version: Sun May  8 16:24:02 CEST 2016
 */

package uebung11.aufgabe02;

/**
 * @author msuess
 */
public interface PriorityQueueInterface<K extends Comparable<? super K>, V> {

  int size();

  boolean isEmpty();

  Entry<K, V> min() throws EmptyPriorityQueueException;

  void insert(K key, V value);

  Entry<K, V> removeMin() throws EmptyPriorityQueueException;

  void print();
}