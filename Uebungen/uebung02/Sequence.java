package uebung02;


public class Sequence {
  public static void main(String[] args) {
    
    // 1,5,9,13,17,?
    System.out.println("expected=21");
    print(iterative(1, 4, 6));
    print(explicite(4, 3, 6));
    print(recursive(1, 4, 6));
    
    // sum total
    // 1 + 5 + 9 + 13 + 17 + 21 = ?
    System.out.println("expected=66");
    print(sumIterative(1,4,6));
    print(sumExplicite(4, 3, 6));
    print(sumRecursive(1, 4, 6));
  }
  
  public static void print(int num){
    System.out.println(num);
  }
  
  public static int iterative(int a, int d, int n){
    for(int i=2; i<=n;i++){
      a+=d;
    }
    return a;
  }
  
  public static int sumIterative(int a, int d, int n){
    int sum = 0;
    for(int i=1; i<=n;i++){
      sum+=iterative(a,d,i);
    }
    return sum;
  }
  
  public static int explicite(int d, int c, int n){
    return d*n-c;
  }
  
  public static int sumExplicite(int d, int c, int n){
    return n*(explicite(d, c, 1)+explicite(d, c, n))/2;
  }
  
  public static int recursive(int a, int d, int n){
    if(n == 1){
      return a;
    }else{
      return recursive(a, d, n - 1) + d;
    }
  }
  
  public static int sumRecursive(int a, int d, int n){
    if(n == 1){
      return a;
    }else{
      return sumRecursive(a, d, n - 1) + recursive(a, d, n);
    }
  }
  
}
