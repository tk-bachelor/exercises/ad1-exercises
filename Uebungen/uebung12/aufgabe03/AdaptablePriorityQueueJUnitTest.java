/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 16 15:14:43 CEST 2016
 */

package uebung12.aufgabe03;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import uebung12.aufgabe02.Entry;
import uebung12.aufgabe02.FullPriorityQueueException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdaptablePriorityQueueJUnitTest {

  AdaptablePriorityQueue<Integer, String> apq;

  @Before
  public void setUp() {
    apq = new AdaptablePriorityQueue<>(3);
  }

  @Test
  public void test01_Insert() throws FullPriorityQueueException {
    Entry<Integer, String> e2 = apq.insert(2, "Two");
    assertEquals(1, apq.size());
    assertSame(e2, apq.min());
    assertEquals("[null, (2,Two), null, null]", apq.toString());
    apq.insert(3, "Three");
    assertEquals(2, apq.size());
    assertSame(e2, apq.min());
    assertEquals("[null, (2,Two), (3,Three), null]", apq.toString());
    Entry<Integer, String> e1 = apq.insert(1, "One");
    assertEquals(3, apq.size());
    assertEquals("[null, (1,One), (3,Three), (2,Two)]", apq.toString());
    assertSame(e1, apq.min());
  }

  @Test
  public void test02_RemoveMin() throws FullPriorityQueueException {
    Entry<Integer, String> e2 = apq.insert(2, "Two");
    Entry<Integer, String> e3 = apq.insert(3, "Three");
    Entry<Integer, String> e1 = apq.insert(1, "One");
    assertSame(e1, apq.removeMin());
    assertEquals("[null, (2,Two), (3,Three), null]", apq.toString());
    assertSame(e2, apq.removeMin());
    assertEquals("[null, (3,Three), null, null]", apq.toString());
    assertSame(e3, apq.removeMin());
    assertEquals("[null, null, null, null]", apq.toString());
  }

  @Test(expected = InvalidEntryException.class)
  public void test03_Remove() throws InvalidEntryException,
      FullPriorityQueueException {
    Entry<Integer, String> e2 = apq.insert(2, "Two");
    Entry<Integer, String> e1 = apq.insert(1, "One");
    Entry<Integer, String> e3 = apq.insert(3, "Three");
    assertEquals("[null, (1,One), (2,Two), (3,Three)]", apq.toString());
    assertSame(e1, apq.remove(e1));
    assertEquals("[null, (2,Two), (3,Three), null]", apq.toString());
    assertSame(e2, apq.remove(e2));
    assertEquals("[null, (3,Three), null, null]", apq.toString());
    assertSame(e3, apq.remove(e3));
    assertEquals("[null, null, null, null]", apq.toString());
    assertSame(e3, apq.remove(e3));
  }

  @Test
  public void test04_Init() {
    assertEquals(0, apq.size());
    assertTrue(apq.isEmpty());
    assertEquals("[null, null, null, null]", apq.toString());
    assertNull(apq.removeMin());
  }

  @Test(expected = FullPriorityQueueException.class)
  public void test05_FullPriorityQueueException()
      throws FullPriorityQueueException {
    apq = new AdaptablePriorityQueue<>(1);
    Entry<Integer, String> e1 = apq.insert(1, "One");
    Entry<Integer, String> e2 = apq.insert(2, "Two");
  }

  @Test(expected = InvalidEntryException.class)
  public void test06_InvalidEntryException() throws InvalidEntryException {
    apq.remove(null);
  }

}
