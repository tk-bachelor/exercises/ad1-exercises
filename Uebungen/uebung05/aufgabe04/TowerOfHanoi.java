/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Mar 20 18:07:29 CET 2016
 */

package uebung05.aufgabe04;

public class TowerOfHanoi {

  public void solveHanoi(int n, String to, String from, String u) {
    if(n == 1){
      moveIt(n, from, to);
    }else{
      solveHanoi(n-1, u, from, to);
      moveIt(n, from, to);
      solveHanoi(n-1, to, u, from);
    }
  }

  public void moveIt(int nr, String from, String to) {
    System.out
        .println("Move disc number " + nr + " from " + from + " to " + to);
  }

  public static void main(String args[]) {
    TowerOfHanoi hanoi = new TowerOfHanoi();
    
    int i = 3;
    hanoi.solveHanoi(i, "Sink", "Source", "Workplace");
  }

}

/* Session-Log (SOLL):

$ java uebung05.as.aufgabe04.TowerOfHanoi 2    
Move disc number 1 from source to workplace
Move disc number 2 from source to sink
Move disc number 1 from workplace to sink

*/

 
 
 
 
