/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Thu May 12 11:20:35 CEST 2016
 */

package uebung11.ml.aufgabe03;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JOptionPane;

import gvs.tree.GVSTreeWithCollection;
import uebung11.aufgabe02.Entry;
import uebung11.aufgabe02.PriorityQueue;

public class HuffmanTree {

  private Node<Entry<Long, Character>> root;
  private int size;
  private GVSTreeWithCollection gvsTree = null;

  public HuffmanTree(Collection<Entry<Long, Character>> frequencies, String gvsTitle) {
    // init the communication to the "Graph-Visualization-Server GVS":
    initGVS(gvsTitle);

    size = frequencies.size();

    PriorityQueue<Long, Node<Entry<Long, Character>>> queue = new PriorityQueue<Long, Node<Entry<Long, Character>>>();
    Iterator<Entry<Long, Character>> it = frequencies.iterator();
    Entry<Long, Character> e;
    while (it.hasNext()) {
      e = it.next();
      queue.insert(e.getKey(), new Node<Entry<Long, Character>>(e));
    }
    Entry<Long, Node<Entry<Long, Character>>> a, b;
    Node<Entry<Long, Character>> n;
    long value;
    while (queue.size() > 1) {
      a = queue.removeMin();
      b = queue.removeMin();
      value = a.getKey() + b.getKey();
      n = new Node<Entry<Long, Character>>(new Entry<Long, Character>(value, null), a.getValue(), b
          .getValue());
      queue.insert(value, n);
      // show actual status in GVS:
      gvsTree.add(n);
      gvsTree.add(n.getGVSLeftChild());
      gvsTree.add(n.getGVSRightChild());
      gvsTree.display();
    }
    root = queue.removeMin().getValue();

    gvsTree.disconnect();
  }

  public Node<Entry<Long, Character>> getRoot() {
    return root;
  }

  public void print() {
    int indent = (int) Math.pow(2, 1 + Math.ceil(Math.log(size)
        / (2 * Math.log(2))));
    int lastIndent = -1;
    int ci = 0;
    Node<Entry<Long, Character>> n;
    ArrayList<Node<Entry<Long, Character>>> indices = new ArrayList<Node<Entry<Long, Character>>>();
    ArrayList<Long> indents = new ArrayList<Long>();
    indices.add(root);
    indents.add(Long.valueOf(indent));
    while (indices.size() > 0) {
      n = indices.get(0);
      indices.remove(0);
      indent = indents.get(0).intValue();
      indents.remove(0);
      if (indent < lastIndent) {
        System.out.println();
        ci = 0;
      }
      lastIndent = indent;
      if (n.getLeftChild() != null) {
        indices.add(n.getLeftChild());
        indents.add(Long.valueOf(indent / 2));
      }
      if (n.getRightChild() != null) {
        indices.add(n.getRightChild());
        indents.add(Long.valueOf(indent + indent / 2));
      }

      for (; ci < indent; ci++) {
        System.out.print("    ");
      }
      System.out.print(n.getValue().toString());

    }
  }

  void initGVS(String title) {
    gvsTree = new GVSTreeWithCollection(title);
    if ((System.getProperty("NoGVS") == null) && (!gvsTree.isConnected())) {
      String[] text = { "Connection to GVS-Server failed!\n",
          "Start \"GVS_Server_v1.4.jar\" first.",
          "(located in \"7_Zusatzmaterial/GraphsVisualizationService-GVS_v1.6.zip\")" };
      JOptionPane.showMessageDialog(null, text);
      System.exit(2);
    }
  }

  public static void main(String[] args) {
    ArrayList<Entry<Long, Character>> frequencies = new ArrayList<Entry<Long, Character>>();
    frequencies.add(new Entry<Long, Character>(3L, 'a'));
    frequencies.add(new Entry<Long, Character>(4L, 'e'));
    frequencies.add(new Entry<Long, Character>(5L, 'i'));
    frequencies.add(new Entry<Long, Character>(6L, 'o'));
    frequencies.add(new Entry<Long, Character>(7L, 'u'));
    HuffmanTree tree = new HuffmanTree(frequencies, "HuffmanTree");
    tree.print();
  }
}

/* Session-Log:


                               25
                11                                14
        5: i                6: o7: u                                                7
                                    3: a                                                                        4: e

*/

 
 
 
 
