/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 16 15:14:43 CEST 2016
 */

package uebung12.aufgabe03;

import uebung12.aufgabe02.Entry;
import uebung12.aufgabe02.PriorityQueue;

/**
 * A heap-based (array-implementation) Adaptable-Priority-Queue (APQ) with fixed
 * length.
 */
public class AdaptablePriorityQueue<K extends Comparable<? super K>, V>
    extends PriorityQueue<K, V> {

  public static class APQEntry<K extends Comparable<? super K>, V>
      extends PriorityQueue.PQEntry<K, V> {

    protected int index;

    protected APQEntry(K key, V value, int index) {
      super(key, value);
      this.index = index;
    }

    void setKey(K key) {
      super.key = key;
    }

    void setValue(V value) {
      super.value = value;
    }

  }

  AdaptablePriorityQueue(int maxSize) {
    super(maxSize);
  }

  /**
   * Replaces the key in the given entry and ensures the inner-order afterwards.
   * 
   * @param entry
   *          The entry where to replace the key.
   * @param newKey
   *          The new key.
   * @return The old key.
   * @throws InvalidEntryException
   *           In case of an entry which does not belong the the APQ or for
   *           null.
   */
  public K replaceKey(Entry<K, V> entry, K newKey)
      throws InvalidEntryException {
    APQEntry<K, V> apqEntry = verifyEntry(entry);
    
    K oldKey = entry.getKey();
    apqEntry.setKey(newKey);
    adjustHeapOrder(apqEntry);
    
    return oldKey;
  }

  private void adjustHeapOrder(APQEntry<K, V> apqEntry) {
    int index = apqEntry.index;
    int parentIndex = index / 2;
    
    // parent is greater than child
    if((parentIndex > 0) && (heapArray[parentIndex].compareTo(apqEntry) > 0)){
      upheap(index);
    }else{
      downheap(index);
    }
  }

  private APQEntry<K, V> verifyEntry(Entry<K, V> entry) throws InvalidEntryException {
    if(!(entry instanceof APQEntry)){
      throw new InvalidEntryException("not a APQ Entry");
    }
    APQEntry<K, V> apqEntry = (APQEntry<K, V>) entry;
    
    if(apqEntry.index == -1){
      throw new InvalidEntryException("invalid index");
    }
    return apqEntry;
    
  }

  public V replaceValue(Entry<K, V> entry, V newValue)
      throws InvalidEntryException {

    APQEntry<K, V> apqEntry = verifyEntry(entry);
    
    V oldValue = apqEntry.getValue();
    apqEntry.setValue(newValue);
    
    return oldValue;
  }

  @Override
  public Entry<K, V> removeMin() {
    APQEntry<K, V> apqEntry = (APQEntry<K, V>) super.removeMin();
    if(apqEntry != null){
      // make it invalid
      apqEntry.index = -1;
      
      if(heapArray[1] != null){
        ((APQEntry<K, V>) heapArray[1]).index = 1;
      }
    }    
    
    return apqEntry;
  }

  /**
   * Removes the given entry from this APQ.
   * 
   * @param entry
   *          The entry to remove.
   * @return The removed entry.
   * @throws InvalidEntryException
   *           In case of an entry which does not belong the the APQ or for
   *           null.
   */
  public Entry<K, V> remove(Entry<K, V> entry) throws InvalidEntryException {
    APQEntry<K, V> apqEntry = verifyEntry(entry);
    
    int index = apqEntry.index;
    heapArray[index] = heapArray[last];
    ((APQEntry<K, V>) heapArray[index]).index = index;
    heapArray[last] = null;
    last--;
    downheap(index);
    apqEntry.index = -1;
    
    return apqEntry;
  }


  /**
   * Factory-Method for new entries.
   * 
   * @param key
   *          The key of the new entry.
   * @param value
   *          The value of the new entry.
   * @return The new created Entry.
   */
  @Override
  protected PQEntry<K, V> newEntry(K key, V value) {
    return new APQEntry<K, V>(key, value, last);
  }

  /**
   * If GVS is in use: The actual state will be shown on GVS.
   */
  public void displayOnGVS() {
  }
  
  
  @Override
  protected void swap(int parent, int child) {
    super.swap(parent, child);
    
    // update the references
    ((APQEntry<K, V>) heapArray[parent]).index = parent;
    ((APQEntry<K, V>) heapArray[child]).index = child;
  } 

}
