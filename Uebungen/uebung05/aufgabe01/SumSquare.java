/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
  * Version: Sun Mar 20 18:07:29 CET 2016
*/

package uebung05.aufgabe01;

import java.util.Scanner;

public class SumSquare {

  public static int sumSquare(int n) {
    if(n==0){
      return 1;
    }else{
      return (int) Math.pow(2, n) + sumSquare(n-1);
    }
  }

  public static void main(String[] args) {
    System.out.print("n : ");
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    scanner.close();
    long start = System.nanoTime();
    int result = sumSquare(n);
    long stop = System.nanoTime();
    System.out.println("sumSquare(" + n + ") = " + result);
    System.out.println("Time: " + (stop-start)+"ns");
  }

}

/* Session-Log:

n : 3
sumSquare(3) = 15

*/

 
 
 
 
