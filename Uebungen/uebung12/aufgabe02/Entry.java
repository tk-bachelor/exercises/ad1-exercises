/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 16 15:14:36 CEST 2016
 */

package uebung12.aufgabe02;

public interface Entry<K, V> {

  K getKey();

  V getValue();
  
}
