/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 22 19:20:03 CEST 2016
 */

package uebung12.ml.aufgabe02;


/**
 * A heap-based (array-implementation) Priority-Queue with fixed length.
 */
public class PriorityQueue<K extends Comparable<? super K>, V> {
  
  
  protected PQEntry<K, V>[] heapArray;

  /** Points to the last element in the heap. */
  protected int last = 0;
  
  public static class PQEntry<K extends Comparable<? super K>, V> implements
      Entry<K, V>, Comparable<PQEntry<K, V>> {

    protected K key;
    protected V value;

    protected PQEntry(K key, V value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public K getKey() {
      return key;
    }

    @Override
    public V getValue() {
      return value;
    }

    @Override
    public int compareTo(PQEntry<K, V> other) {
      return this.key.compareTo(other.key);
    }
    
    @Override
    public String toString() {
      return "("+key+","+value+")";
    }

  }


  @SuppressWarnings("unchecked")
  public PriorityQueue(int maxSize) {
    heapArray = new PQEntry[maxSize + 1];
  }

  public Entry<K, V> insert(K key, V value) throws FullPriorityQueueException {
    if (last == heapArray.length-1) {
      throw new FullPriorityQueueException();
    }
    last++;
    PQEntry<K, V> e = newEntry(key, value);
    heapArray[last] = e;
    upheap(last);
    return e;
  }
  
  public Entry<K, V> min() {
    if (last == 0) {
      return null;
    } else 
      return heapArray[1];
  }

  public Entry<K, V> removeMin() {
    if (last == 0) {
      return null;
    }
    PQEntry<K, V> result = heapArray[1];
    heapArray[1] = heapArray[last];
    heapArray[last] = null;
    last--;
    downheap(1);
    return result;
  }

  protected void upheap(int child) {
    int parent = child / 2;
    while ((parent > 0) && (heapArray[parent].compareTo(heapArray[child]) > 0)) {
      swap(parent, child);
      // go one step higher in the tree
      child = parent;
      parent = child / 2;
    }
  }

  protected void downheap(int actualIndex) {
    int leftIndex = actualIndex * 2;
    while (leftIndex < heapArray.length && heapArray[leftIndex] != null) {
      int smallerChildIndex = leftIndex;
      int rightIndex = leftIndex + 1;
      if (rightIndex < heapArray.length && heapArray[rightIndex] != null) {
        if (heapArray[rightIndex].compareTo(heapArray[leftIndex]) < 0) {
          smallerChildIndex = rightIndex;
        }
      }
      if (heapArray[smallerChildIndex].compareTo(heapArray[actualIndex]) >= 0) {
        break;
      } else {
        swap(actualIndex, smallerChildIndex);
        actualIndex = smallerChildIndex;
        leftIndex = actualIndex * 2;
      }
    }
  }

  protected void swap(int parent, int child) {
      PQEntry<K, V> tmp = heapArray[parent];
      heapArray[parent] = heapArray[child];
      heapArray[child] = tmp;
  }
  
  /**
   * Factory-Method for new entries.
   * @param key The key of the new entry.
   * @param value The value of the new entry.
   * @return The new created Entry.
   */
  protected PQEntry<K, V> newEntry(K key, V value) {
    return new PQEntry<K, V>(key, value);
  }

  public boolean isEmpty() {
    return size() == 0;
  }
  
  public int size() {
    return last;
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("[");
    for (int i = 0; i < heapArray.length; i++) {
      PQEntry<K, V> e = heapArray[i];
      if (e != null) {
        sb.append(e);
      } else {
        sb.append("null");
      }
      if (i < heapArray.length-1) {
        sb.append(", ");
      }
    }
    sb.append("]");
    return sb.toString();
  }

  public void print() {
    System.out.println(toString());
  }
  
  /**
   * If GVS is in use: The actual state will be shown on GVS.
   */
  public void displayOnGVS() {
  }

} 

 
