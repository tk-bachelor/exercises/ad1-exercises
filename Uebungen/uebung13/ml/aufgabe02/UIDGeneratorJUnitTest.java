/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Mon May 23 10:34:36 CEST 2016
 */

package uebung13.ml.aufgabe02;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UIDGeneratorJUnitTest {

  private static Map<String, String> uids;
  private UIDGenerator gen;

  @SuppressWarnings("unchecked")
  @BeforeClass
  public static void init() throws Exception {
    File f = new File(UIDGenerator.SAVE_FILE);
    if (f.exists()) {
      FileInputStream fis = new FileInputStream(f);
      ObjectInputStream ois = new ObjectInputStream(fis);
      uids = (Map<String, String>) ois.readObject();
      ois.close();
    } else {
      String errMsg = "File can\'t be opened: " + UIDGenerator.SAVE_FILE;
      System.err.println(errMsg);
      fail(errMsg);
    }
    // make sure that our test-case "xabcdefg" is not already in the map
    if (uids.get("xabcdefg") != null) {
      String errMsg = "UserId \"xabcdefg\" should not be already in the map, is the test-case! Please remove it.";
      System.err.println(errMsg);
      fail(errMsg);
    }
  }

  @Before
  public void initTest() {
    gen = new UIDGenerator(uids);
  }

  @Test
  public void test1StandardCase() {
    String firstName = "Xyz";
    String lastName = "Abcdefghijk";
    int size = uids.size();
    String uid = gen.generateUID(firstName, lastName);
    assertEquals("generateUID() must not change the Map!", size, uids.size());
    assertEquals("xabcdefg", uid);
    uids.put(uid, firstName + " " + lastName);
    uid = gen.generateUID(firstName, lastName);
    assertEquals("x1abcdef", uid);
    uids.put(uid, firstName + " " + lastName);
  }

  @Test
  public void test2ShortName() {
    String firstName = "Xyz";
    String lastName = "Abc";
    String uid = gen.generateUID(firstName, lastName);
    assertEquals("xabc", uid);
  }

  @Test
  public void test3ManySameName() {
    String firstName = "Xyz";
    String lastName = "Abcdefghijk";
    String uid = gen.generateUID(firstName, lastName);
    assertEquals("x2abcdef", uid); // be aware: "xabcdefg" and "x1abcdef" has been inserted above
    uids.put(uid, firstName + " " + lastName);
    for (int i = 3; i <= 1000; i++) {
      uid = gen.generateUID(firstName, lastName);
      uids.put(uid, firstName + " " + lastName);
    }
    assertEquals("x1000abc", uid);
  }
  
  @Test
  public void test4SpecialCharacters() {
    String firstName = "Xyz";
    String lastName = "AbCdefghijk";
    String uid = gen.generateUID(firstName, lastName);
    assertEquals("x1001abc", uid); // be aware: "x1000abc" has been inserted above
    uids.put(uid, firstName + " " + lastName);
    lastName = "Ab-Cdefghijk";
    uid = gen.generateUID(firstName, lastName);
    assertEquals("x1002abc", uid);
    uids.put(uid, firstName + " " + lastName);
    lastName = "Ab(Cdefghij)";
    uid = gen.generateUID(firstName, lastName);
    assertEquals("x1003abc", uid);
    uids.put(uid, firstName + " " + lastName);
    lastName = "Ab Cdefghijk";
    uid = gen.generateUID(firstName, lastName);
    assertEquals("x1004abc", uid);
    uids.put(uid, firstName + " " + lastName);
  }

}
 
 
 
