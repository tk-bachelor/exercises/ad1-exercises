/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 1'
 * Version: Sun Apr 17 20:05:29 CEST 2016
 */

package uebung08.aufgabe01;

/**
 * Runtime exception thrown when one tries to perform operation on an empty
 * queue.
 */

public class EmptyQueueException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public EmptyQueueException(String err) {
    super(err);
  }
}